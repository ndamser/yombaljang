package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.NiveauDto;
import com.ndamser.yombaljang.service.NiveauService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/niveaux")
@AllArgsConstructor
@Slf4j
public class NiveauController {

    private final NiveauService niveauService;

    // ------------------- tout les niveaux-----------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<NiveauDto>> getNiveaux() {
        return ResponseEntity.status(HttpStatus.OK).body(niveauService.findAll());
    }
}
