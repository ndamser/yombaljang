package com.ndamser.yombaljang.controller;

import com.ndamser.yombaljang.dto.EleveDto;
import com.ndamser.yombaljang.service.EleveService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/eleves")
@AllArgsConstructor
@Slf4j
public class EleveController {

    private final EleveService eleveService;

    // -------------------recuperer tout les eleves---------------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<EleveDto>> listEleves() {
        return ResponseEntity.status(HttpStatus.OK).body(eleveService.findActive());
    }

    // -------------------recuperer les eleves d'une classe---------------------------------------------
    @GetMapping(value = "/{nomClasse}")
    public ResponseEntity<List<EleveDto>> getEleveFormClasse(@PathVariable String nomClasse) {
        return ResponseEntity.status(HttpStatus.OK).body(eleveService.findElevesParClasse(nomClasse));
    }

    // -------------------sauvgarder un eleve---------------------------------------------
    @PostMapping(value = "/")
    public ResponseEntity<EleveDto> saveEleve(@RequestBody EleveDto eleveDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(eleveService.saveEleve(eleveDto));

    }

    // -------------------sauvgarder leleve avec le relation entre eleve et responsable---------------------------------------------
    @PostMapping(value = "/{cin}/{idNiveau}/{cin2}")
    public ResponseEntity<EleveDto> createEleve(@RequestBody EleveDto eleveDto, @PathVariable String cin, @PathVariable Long idNiveau, @PathVariable String cin2) {

        return ResponseEntity.status(HttpStatus.CREATED).body(eleveService.save(eleveDto, cin, idNiveau, cin2));
    }

    // -------------------recuperer l'eleve par son email---------------------------------------------
    @GetMapping(value = "/email/{email}")
    public ResponseEntity<EleveDto> getEleveParEmail(@PathVariable String email) {
        return ResponseEntity.status(HttpStatus.OK).body(eleveService.findByemail(email));
    }

    // -------------------recuperer l'eleve par son id---------------------------------------------
    @GetMapping(value = "/one/{id}")
    public ResponseEntity<EleveDto> getEleveParId(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(eleveService.findOne(id));
    }

    // -------------------desable l'eleve par son id---------------------------------------------
    @GetMapping(value = "/desable/{id}")
    public ResponseEntity<EleveDto> desableEleveParId(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(eleveService.desableById(id));
    }

    // ------------------- recuperer l'eleve apartir dune note ----------------------------------------
    @GetMapping(value = "/notes/{idNote}/eleve")
    public ResponseEntity<EleveDto> getEleveFromNote(@PathVariable Long idNote) {
        return ResponseEntity.status(HttpStatus.OK).body(eleveService.findEleve(idNote));
    }

}
