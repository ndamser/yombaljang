package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.GestionnaireDto;
import com.ndamser.yombaljang.service.GestionnaireService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/gestionnaires")
@AllArgsConstructor
@Slf4j
public class GestionnaireController {

    private final GestionnaireService gestionnaireService;

    // -------------------sauvgarder un gestionnaire---------------------------------------------
    @PostMapping(value = "/")
    public ResponseEntity<GestionnaireDto> createGestionnaire(@RequestBody GestionnaireDto gestionnaireDto) {

        return ResponseEntity.status(HttpStatus.CREATED).body(gestionnaireService.saveGestioonaire(gestionnaireDto));
    }

    // -------------------get gestionnaire---------------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<GestionnaireDto>> getGestionnaire() {

        return ResponseEntity.status(HttpStatus.OK).body(gestionnaireService.getAll());
    }

    // -------------------desable Gestionnaire par son id---------------------------------------------
    @GetMapping(value = "/{id}/desable")
    public ResponseEntity<GestionnaireDto> desableGestionnaireParId(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(gestionnaireService.save(id));
    }
}
