package com.ndamser.yombaljang.controller;

import com.ndamser.yombaljang.dto.CahierTexteDto;
import com.ndamser.yombaljang.service.CahierDeTexteService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cahierdetexte")
@AllArgsConstructor
@Slf4j
public class CahierDeTexteController {

    private final CahierDeTexteService cahierDeTexteService;

    // ------------------- cahier de texte par matiere et classe----------------------------------------
    @PostMapping(value = "/{matiere}/{classe}")
    public ResponseEntity<CahierTexteDto> createCahierTexte(@RequestBody CahierTexteDto cahierTexteDto, @PathVariable String matiere, @PathVariable String classe) {
        return ResponseEntity.status(HttpStatus.CREATED).body(cahierDeTexteService.saveCahierText(cahierTexteDto, matiere, classe));
    }

    // ------------------- cahier de texte d'une classe----------------------------------------
    @GetMapping(value = "/classe/{nomClasse}")
    public ResponseEntity<CahierTexteDto> getCahierTextePerClasse(@PathVariable String nomClasse) {
        return ResponseEntity.status(HttpStatus.OK).body(cahierDeTexteService.findCahierParClasse(nomClasse));
    }

    // -------------------  cahier de texte par matiere---------------------------------------
    @GetMapping(value = "/matiere/{nomMatiere}")
    public ResponseEntity<List<CahierTexteDto>> getCahierTextePerMatiere(@PathVariable String nomMatiere) {
        return ResponseEntity.status(HttpStatus.OK).body(cahierDeTexteService.findCahierParMatiere(nomMatiere));
    }

    // ------------------- tout les cahiers de texte----------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<CahierTexteDto>> getAllCahierTexte() {

        return ResponseEntity.status(HttpStatus.OK).body(cahierDeTexteService.findAll());
    }

    // ------------------- cahier de texte par id ----------------------------------------
    @GetMapping(value = "/{idCahierTexte}")
    public ResponseEntity<CahierTexteDto> getCahierTexteParId(@PathVariable Long idCahierTexte) {
        return ResponseEntity.status(HttpStatus.OK).body(cahierDeTexteService.findOne(idCahierTexte));
    }

    // ------------------- Supprimer un cahier de texte----------------------------------------
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteCahierDeTexte(@PathVariable Long id) {
        cahierDeTexteService.delele(id);
        return ResponseEntity.status(HttpStatus.OK).body("Cahier de Texte supprimé avec succes");
    }

}
