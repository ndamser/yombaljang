package com.ndamser.yombaljang.controller;

import com.ndamser.yombaljang.dto.ContactDto;
import com.ndamser.yombaljang.service.ContactService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/contact")
@AllArgsConstructor
@Slf4j
public class ContactController {

    private final ContactService contactService;

    // ------------------- cree un contact associe a un fournisseur----------------------------------------
    @PostMapping(value = "/")
    public ResponseEntity<ContactDto> createContact(@RequestBody ContactDto contactDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(contactService.save(contactDto));
    }

    // -------------------return contact fromfournisseur par son id---------------------------------------------
    @GetMapping(value = "/{id}/contacts")
    public ResponseEntity<ContactDto> contactFromFournisseurParId(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(contactService.findContactFromFournisseur(id));
    }

}
