package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.ResponsableDto;
import com.ndamser.yombaljang.service.ResponsableService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/responsables")
@AllArgsConstructor
@Slf4j
public class ResponsableController {

    private final ResponsableService responsableService;

    //---------------------------------get per id-------------------------------------------
    @GetMapping(value = "/one/{id}")
    public ResponseEntity<ResponsableDto> getResponsableParId(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(responsableService.findOne(id));
    }

    // ------------------- recuperer un responsable par son cin---------------------------------------------
    @GetMapping(value = "/{cin}")
    public ResponseEntity<ResponsableDto> getrespoParCin(@PathVariable String cin) {

        return ResponseEntity.status(HttpStatus.OK).body(responsableService.findParCin(cin));
    }


    // ------------------- sauvgarder un responsable------------------------------------------------
    @PostMapping(value = "/")
    public ResponseEntity<ResponsableDto> saveresponsable(@RequestBody ResponsableDto responsableDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(responsableService.saveRespo(responsableDto));
    }

    // ------------------- recuperer tout les responsables ----------------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<ResponsableDto>> getAllResponsable() {

        return ResponseEntity.status(HttpStatus.OK).body(responsableService.findAll());
    }

    // ------------------- modifier un responsable----------------------------------------------
    @PutMapping(value = "/")
    public ResponseEntity<ResponsableDto> updateResponsable(@RequestBody ResponsableDto responsableDto) {
        return ResponseEntity.status(HttpStatus.OK).body(responsableService.update(responsableDto));
    }

}
