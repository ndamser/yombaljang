package com.ndamser.yombaljang.controller;

import com.ndamser.yombaljang.dto.FournisseurDto;
import com.ndamser.yombaljang.service.FournisseurService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/fournisseurs")
@AllArgsConstructor
@Slf4j
public class FournisseurController {

    private final FournisseurService fournisseurService;

    // -------------------Recuperer liste des fournisseurs---------------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<FournisseurDto>> listFournisseur() {

        return ResponseEntity.status(HttpStatus.OK).body(fournisseurService.findActive());
    }

    // -------------------Recuperer un fournisseur---------------------------------------------
    @GetMapping(value = "/{id}")
    public ResponseEntity<FournisseurDto> Fournisseur(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(fournisseurService.findOne(id));
    }

    // -------------------Creer un nv fournisseur-------------------------------------------
    @PostMapping(value = "/{cin}")
    public ResponseEntity<FournisseurDto> createFournisseur(@RequestBody FournisseurDto fournisseurDto, @PathVariable String cin) {
        return ResponseEntity.status(HttpStatus.CREATED).body(fournisseurService.saveFournisseur(fournisseurDto, cin));
    }

    // -------------------desable une facture par son id---------------------------------------------
    @GetMapping(value = "/desable/{id}")
    public ResponseEntity<FournisseurDto> desableFournisseurParId(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(fournisseurService.save(id));
    }

    /*//--------------------------une facture-----------------------------------------
    @GetMapping(value = "/factures/{id}")
    public ResponseEntity<FournisseurDto> Fournisseur(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(fournisseurService.findFournisseurFromFacture(id));
    }*/

}
