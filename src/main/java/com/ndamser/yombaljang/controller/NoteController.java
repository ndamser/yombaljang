package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.NoteDto;
import com.ndamser.yombaljang.service.NoteService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/notes")
@AllArgsConstructor
@Slf4j
public class NoteController {

    private final NoteService noteService;


    // ------------------- Sauvgarder  une note----------------------------------------
    @PostMapping(value = "/{matiere}/{eleve}")
    public ResponseEntity<NoteDto> createNote(@RequestBody NoteDto noteDto, @PathVariable String matiere, @PathVariable String eleve) {
        return ResponseEntity.status(HttpStatus.CREATED).body(noteService.saveNote(noteDto, matiere, eleve));
    }

    // ------------------- recuperer les notes par classe et matiere----------------------------------------
    @GetMapping(value = "/{nomClasse}/{nomMatiere}")
    public ResponseEntity<List<NoteDto>> getNoteParMatiereAndClasse(@PathVariable String nomClasse, @PathVariable String nomMatiere) {
        return ResponseEntity.status(HttpStatus.OK).body(noteService.findByClasseAndMatiere(nomClasse, nomMatiere));
    }

    // ------------------- recupere les notes d'un eleve par matiere----------------------------------------
    @GetMapping(value = "/eleve/{matricule}/{nomMatiere}")
    public ResponseEntity<List<NoteDto>> getNoteParMatiere(@PathVariable String matricule, @PathVariable String nomMatiere) {
        return ResponseEntity.status(HttpStatus.OK).body(noteService.findByEleveMatier(matricule, nomMatiere));
    }

    // ------------------- Supprimer une note----------------------------------------
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteNote(@PathVariable Long id) {
        noteService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Note supprimé avec succes");
    }

    // ------------------- recuperer  une note----------------------------------------
    @GetMapping(value = "/{idNote}")
    public ResponseEntity<NoteDto> getNoteParId(@PathVariable Long idNote) {
        return ResponseEntity.status(HttpStatus.OK).body(noteService.findOne(idNote));
    }
}
