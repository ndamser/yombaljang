package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.ObservationDto;
import com.ndamser.yombaljang.service.ObservationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/observations")
@AllArgsConstructor
@Slf4j
public class OservationController {

    private final ObservationService observationService;

    // ------------------- sauvgarder lobservation par eleve et matiere----------------------------------------
    @PostMapping(value = "/{matiere}/{eleve}")
    public ResponseEntity<ObservationDto> createObservation(@RequestBody ObservationDto observationDto, @PathVariable String matiere, @PathVariable String eleve) {
        return ResponseEntity.status(HttpStatus.CREATED).body(observationService.saveObservation(observationDto, matiere, eleve));
    }

    // ------------------- les observation d'un eleve----------------------------------------
    @GetMapping(value = "/eleve/{matricule}")
    public ResponseEntity<List<ObservationDto>> getObservationsParEleve(@PathVariable String matricule) {
        return ResponseEntity.status(HttpStatus.OK).body(observationService.findByEleve(matricule));
    }

    // ------------------- observation par id----------------------------------------
    @GetMapping(value = "/{idObservation}")
    public ResponseEntity<ObservationDto> getObservationById(@PathVariable Long idObservation) {
        return ResponseEntity.status(HttpStatus.OK).body(observationService.findOne(idObservation));
    }

    // ------------------- Supprimer une observation-----------------------------------------
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteObservation(@PathVariable Long id) {
        observationService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Observation supprimée avec succes");
    }
}
