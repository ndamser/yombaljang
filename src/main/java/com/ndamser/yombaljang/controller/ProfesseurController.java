package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.ProfesseurDto;
import com.ndamser.yombaljang.service.ProfesseurService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/professeurs")
@AllArgsConstructor
@Slf4j
public class ProfesseurController {

    private final ProfesseurService professeurService;

    // -------------------sauvgarder un professeur---------------------------------------------
    @PostMapping(value = "/")
    public ResponseEntity<ProfesseurDto> createeleve(@RequestBody ProfesseurDto professeurDto) {

        return ResponseEntity.status(HttpStatus.CREATED).body(professeurService.saveProfesseur(professeurDto));
    }

    @GetMapping(value = "/")
    public ResponseEntity<List<ProfesseurDto>> getProf() {
        return ResponseEntity.status(HttpStatus.OK).body(professeurService.findActive());
    }

    // -------------------desable prof par son id---------------------------------------------
    @GetMapping(value = "/{id}/desable")
    public ResponseEntity<ProfesseurDto> desableProfesseurParId(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(professeurService.save(id));
    }

}
