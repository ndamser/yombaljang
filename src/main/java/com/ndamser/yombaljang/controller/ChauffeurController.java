package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.ChauffeurDto;
import com.ndamser.yombaljang.service.ChauffeurService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/chauffeurs")
@AllArgsConstructor
@Slf4j
public class ChauffeurController {

    private final ChauffeurService chauffeurService;

    // -------------------sauvgarder un chauffeur---------------------------------------------
    @PostMapping(value = "/")
    public ResponseEntity<ChauffeurDto> createchauffeurs(@RequestBody ChauffeurDto chauffeurDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(chauffeurService.saveChauffeur(chauffeurDto));
    }

    // -------------------get chauffeur---------------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<ChauffeurDto>> getchauffeur() {
        return ResponseEntity.status(HttpStatus.OK).body(chauffeurService.findActive());
    }

    // -------------------desable Chauffeur par son id---------------------------------------------
    @GetMapping(value = "/{id}/desable")
    public ResponseEntity<ChauffeurDto> desableCahuffeurParId(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(chauffeurService.save(id));
    }
}
