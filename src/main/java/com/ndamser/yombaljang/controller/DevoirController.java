package com.ndamser.yombaljang.controller;

import com.ndamser.yombaljang.dto.DevoirDto;
import com.ndamser.yombaljang.service.DevoirService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/devoir")
@AllArgsConstructor
@Slf4j
public class DevoirController {

    private DevoirService devoirService;

    // ------------------- sauvgarder un devoir par matiere et classe---------------------------------------
    @PostMapping(value = "/{matiere}/{classe}")
    public ResponseEntity<DevoirDto> saveDevoir(@RequestBody DevoirDto devoirDto, @PathVariable String matiere, @PathVariable String classe) {
        return ResponseEntity.status(HttpStatus.CREATED).body(devoirService.save(devoirDto, matiere, classe));
    }

    // ------------------- modifier un devoir----------------------------------------
    @PutMapping(value = "/")
    public ResponseEntity<DevoirDto> updateDevoir(@RequestBody DevoirDto devoirDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(devoirService.update(devoirDto));
    }

    // ------------------- recuperer les devoirs par matiere---------------------------------------
    @GetMapping(value = "/matiere/{nomMatiere}")
    public ResponseEntity<List<DevoirDto>> getDevoirsParMatiere(@PathVariable String nomMatiere) {
        return ResponseEntity.status(HttpStatus.OK).body(devoirService.findDevoirsParMatiere(nomMatiere));
    }

    // ------------------- recuperer les devoirs d'un eleve---------------------------------------
    @GetMapping(value = "/eleve/{matricule}")
    public ResponseEntity<List<DevoirDto>> getDevoirsParEleve(@PathVariable String matricule) {
        return ResponseEntity.status(HttpStatus.OK).body(devoirService.findDevoirsParEleve(matricule));
    }

    // ------------------- recuperer un devoir par son id----------------------------------------
    @GetMapping(value = "/{idDevoir}")
    public ResponseEntity<DevoirDto> getDevoirParId(@PathVariable Long idDevoir) {
        return ResponseEntity.status(HttpStatus.OK).body(devoirService.findOne(idDevoir));
    }

    // ------------------- Supprimer un devoir-----------------------------------------
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteDevoir(@PathVariable Long id) {
        devoirService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Devoir supprimé avec succes");
    }
}
