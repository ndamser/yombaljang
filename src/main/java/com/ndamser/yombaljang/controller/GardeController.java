package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.GardeDto;
import com.ndamser.yombaljang.dto.ProfesseurDto;
import com.ndamser.yombaljang.service.GardeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/gardes")
@AllArgsConstructor
@Slf4j
public class GardeController {

    private final GardeService gardeService;

    // ------------------- sauvgarder une garde (prof,aide)-----------------------------------------------
    @PostMapping(value = "/{matricule}/{matriculeAide}")
    public ResponseEntity<GardeDto> createGarde(@RequestBody GardeDto gardeDto, @PathVariable String cin, @PathVariable String matriculeAide) {
        return ResponseEntity.status(HttpStatus.CREATED).body(gardeService.saveGarde(gardeDto, cin, matriculeAide));
    }

    //------------------------------get gardes----------------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<GardeDto>> getGardes() {
        return ResponseEntity.status(HttpStatus.OK).body(gardeService.findActive());
    }

    // -------------------desable garde par son id---------------------------------------------
    @GetMapping(value = "/{id}/desable")
    public ResponseEntity<GardeDto> desableGarde(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(gardeService.save(id));
    }

    //------------------------------get garde par id----------------------------------------------
    @GetMapping(value = "/{id}")
    public ResponseEntity<GardeDto> getGarde(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(gardeService.findOne(id));
    }

    //------------------------------get garde par id----------------------------------------------
    @GetMapping(value = "/prof/{id}")
    public ResponseEntity<ProfesseurDto> getProfFromGarde(@PathVariable long id) {

        return ResponseEntity.status(HttpStatus.OK).body(gardeService.findProf(id));
    }
}
