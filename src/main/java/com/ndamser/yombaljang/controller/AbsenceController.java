package com.ndamser.yombaljang.controller;

import com.ndamser.yombaljang.dto.AbsenceDto;
import com.ndamser.yombaljang.model.Absence;
import com.ndamser.yombaljang.service.AbsenceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/absences")
@AllArgsConstructor
@Slf4j
public class AbsenceController {


    private final AbsenceService absenceService;

    // -------------------absence d'un eleve  par matiere------------------------------------------
    @PostMapping(value = "/{eleve}/{matiere}")
    public ResponseEntity<AbsenceDto> createabsence(@RequestBody AbsenceDto absenceDto, @PathVariable String eleve, @PathVariable String matiere) {
        return ResponseEntity.status(HttpStatus.CREATED).body(absenceService.saveAbsence(absenceDto, eleve, matiere));
    }

    // -------------------Absence justifier par id------------------------------------------
    @GetMapping(value = "/justifier/{id}")
    public ResponseEntity<Absence> justifier(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(absenceService.justifierAbsaenceById(id));
    }

    // ------------------- les absence d'un eleve-----------------------------------------
    @GetMapping(value = "/eleve/{matricule}")
    public ResponseEntity<List<AbsenceDto>> getabsenceParEleve(@PathVariable String matricule) {
        return ResponseEntity.status(HttpStatus.OK).body(absenceService.getabsenceByEleve(matricule));
    }

    // ------------------- liste absence d'une classe----------------------------------------
    @GetMapping(value = "/classe/{nomclasse}")
    public ResponseEntity<List<AbsenceDto>> getabsenceParClasse(@PathVariable String nomclasse) {
        return ResponseEntity.status(HttpStatus.OK).body(absenceService.findParClasse(nomclasse));
    }

    // ------------------- supprimer une absence-----------------------------------------
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteAbsence(@PathVariable Long id) {
        absenceService.deleteAbsenceById(id);
        return ResponseEntity.status(HttpStatus.OK).body("Absence supprimé avec succes");
    }

}
