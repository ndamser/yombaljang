package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.TarifDto;
import com.ndamser.yombaljang.service.TarifsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tarifs")
@AllArgsConstructor
@Slf4j
public class TarifController {

    private final TarifsService tarifService;

    // ------------------- tout les inscriptions---------------------------------------- //
    @GetMapping(value = "/{service}")
    public ResponseEntity<TarifDto> getMotant(@PathVariable String service) {
        return ResponseEntity.status(HttpStatus.OK).body(tarifService.findByService(service));
    }
}
