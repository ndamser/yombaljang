package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.ExamenDto;
import com.ndamser.yombaljang.service.ExamenService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/examens")
@AllArgsConstructor
@Slf4j
public class ExamenController {
    private final ExamenService examenService;

    // -------------------sauvgarderun examen pour une matiere et classe--------------------------------------------
    @PostMapping(value = "/{matiere}/{classe}")
    public ResponseEntity<ExamenDto> createExamen(@RequestBody ExamenDto examenDto, @PathVariable String matiere, @PathVariable String classe) {
        return ResponseEntity.status(HttpStatus.CREATED).body(examenService.saveExamen(examenDto, matiere, classe));
    }

    // -------------------recuperer  les examens d'un eleve---------------------------------------------
    @GetMapping(value = "/{matricule}")
    public ResponseEntity<List<ExamenDto>> getExamensParEleve(@PathVariable String matricule) {
        return ResponseEntity.status(HttpStatus.OK).body(examenService.findByEleve(matricule));
    }

    // -------------------recuperer  les examens d'un eleve---------------------------------------------
    @GetMapping(value = "/{nomClasse}")
    public ResponseEntity<List<ExamenDto>> getExamensParClasse(@PathVariable String nomClasse) {
        return ResponseEntity.status(HttpStatus.OK).body(examenService.findByClasse(nomClasse));
    }

    // -------------------recuperer les examen dune matiere---------------------------------------------
    @GetMapping(value = "/matiere/{nomMatiere}")
    public ResponseEntity<List<ExamenDto>> getExamenParMatiere(@PathVariable String nomMatiere) {
        return ResponseEntity.status(HttpStatus.OK).body(examenService.findParMatiere(nomMatiere));
    }

    // -------------------get examen par id ---------------------------------------------
    @GetMapping(value = "/id/{idExamen}")
    public ResponseEntity<ExamenDto> getExamenParId(@PathVariable Long idExamen) {
        return ResponseEntity.status(HttpStatus.OK).body(examenService.findOne(idExamen));
    }


    // ------------------- Supprimer un examen-----------------------------------------
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteExamen(@PathVariable Long id) {
        examenService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("Examen supprimé avec succes");
    }
}
