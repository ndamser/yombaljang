package com.ndamser.yombaljang.controller;

import com.ndamser.yombaljang.dto.InscriptionDto;
import com.ndamser.yombaljang.service.InscriptionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/inscriptions")
@AllArgsConstructor
@Slf4j
public class InscriptionController {

    private final InscriptionService inscriptionService;

    // ------------------- cree un contact associe a un fournisseur----------------------------------------
    @PostMapping(value = "/{email}")
    public ResponseEntity<InscriptionDto> createInscription(@RequestBody InscriptionDto inscriptionDto, @PathVariable String email) {

        return ResponseEntity.status(HttpStatus.CREATED).body(inscriptionService.saveInscription(inscriptionDto, email));
    }

    // ------------------- tout les inscriptions----------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<InscriptionDto>> allInscriptions() {
        return ResponseEntity.status(HttpStatus.OK).body(inscriptionService.findAll());
    }

    // -------------------current inscriptions----------------------------------------
    @GetMapping(value = "/{email}")
    public ResponseEntity<InscriptionDto> currentInscriptions(@PathVariable String email) {
        return ResponseEntity.status(HttpStatus.OK).body(inscriptionService.findCurrentInscription(email));
    }

}
