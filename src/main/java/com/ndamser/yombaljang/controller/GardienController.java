package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.GardienDto;
import com.ndamser.yombaljang.service.GardienService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/gardiens")
@AllArgsConstructor
@Slf4j
public class GardienController {

    private final GardienService gardienService;

    // -------------------sauvgarder un gardien---------------------------------------------
    @PostMapping(value = "/")
    public ResponseEntity<GardienDto> createGardien(@RequestBody GardienDto gardienDto) {

        return ResponseEntity.status(HttpStatus.CREATED).body(gardienService.saveGardien(gardienDto));
    }

    // -------------------get gardiens--------------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<GardienDto>> getgardiens() {

        return ResponseEntity.status(HttpStatus.OK).body(gardienService.findActive());
    }

    // -------------------desable gardien par son id---------------------------------------------
    @GetMapping(value = "/{id}/desable")
    public ResponseEntity<GardienDto> desableGardienParId(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(gardienService.save(id));
    }
}
