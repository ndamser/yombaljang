package com.ndamser.yombaljang.controller;

import com.ndamser.yombaljang.dto.RetardDto;
import com.ndamser.yombaljang.service.RetardService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/retard")
@AllArgsConstructor
@Slf4j
public class RetardController {


    private final RetardService retardService;

    // -------------------retard d'un eleve  par matiere------------------------------------------
    @PostMapping(value = "/{eleve}/{matiere}")
    public ResponseEntity<RetardDto> createRetard(@RequestBody RetardDto retardDto, @PathVariable String eleve, @PathVariable String matiere) {
        return ResponseEntity.status(HttpStatus.CREATED).body(retardService.saveRetard(retardDto, eleve, matiere));
    }

    // ------------------- liste de retards  d'une classe----------------------------------------
    @GetMapping(value = "/classe/{nomclasse}")
    public ResponseEntity<List<RetardDto>> getRetardsParClasse(@PathVariable("nomclasse") String nomclasse) {
        return ResponseEntity.status(HttpStatus.OK).body(retardService.findParClasse(nomclasse));
    }

    // ------------------- supprimer un retard ----------------------------------------
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteRetard(@PathVariable Long id) {
        retardService.deleteRetardById(id);
        return ResponseEntity.status(HttpStatus.OK).body("Retard supprimé avec succes");
    }

}
