package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.FemmeDeMenageDto;
import com.ndamser.yombaljang.service.FemmeDeMenageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/femmeDeMenages")
@AllArgsConstructor
@Slf4j
public class FemmeDeMenageController {

    private final FemmeDeMenageService femmeDeMenageService;

    // -------------------sauvgarder un responsable menage-------------------------------------------
    @PostMapping(value = "/")
    public ResponseEntity<FemmeDeMenageDto> createRespoMenage(@RequestBody FemmeDeMenageDto femmeDeMenageDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(femmeDeMenageService.saveFM(femmeDeMenageDto));
    }

    // -------------------get femmeDeMenages---------------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<FemmeDeMenageDto>> getfemmeDeMenages() {

        //return menage.findActive(true) ;
        return ResponseEntity.status(HttpStatus.OK).body(femmeDeMenageService.findActive());
    }

    // -------------------desable femme de menage par son id---------------------------------------------
    @GetMapping(value = "/{id}/desable")
    public ResponseEntity<FemmeDeMenageDto> desableMenageParId(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(femmeDeMenageService.save(id));
    }
}
