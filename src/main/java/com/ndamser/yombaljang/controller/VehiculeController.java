package com.ndamser.yombaljang.controller;

import com.ndamser.yombaljang.dto.VehiculeDto;
import com.ndamser.yombaljang.service.VehiculeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/vehicules")
@AllArgsConstructor
@Slf4j
public class VehiculeController {

    private final VehiculeService vehiculeService;

    @PostMapping(value = "/")
    public ResponseEntity<VehiculeDto> createVehicule(@RequestBody VehiculeDto vehiculeDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
            .body(vehiculeService.save(vehiculeDto));
    }

    @GetMapping(value = "/")
    public ResponseEntity<List<VehiculeDto>> getAllVehicules() {
        return ResponseEntity.status(HttpStatus.OK)
            .body(vehiculeService.getAll());
    }
}
