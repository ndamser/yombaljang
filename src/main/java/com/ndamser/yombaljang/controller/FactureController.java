package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.FactureDto;
import com.ndamser.yombaljang.service.FactureService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/factures")
@AllArgsConstructor
@Slf4j
public class FactureController {

    private final FactureService factureService;

    // -------------------Creer un nv facture-------------------------------------------
    @PostMapping(value = "/{nomFournisseur}")
    public ResponseEntity<FactureDto> createFacture(@RequestBody FactureDto factureDto, @PathVariable String nomFournisseur) {
        return ResponseEntity.status(HttpStatus.CREATED).body(factureService.saveFacture(factureDto, nomFournisseur));

    }

    //--------------------------list of factures ------------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<FactureDto>> listFactures() {
        return ResponseEntity.status(HttpStatus.OK).body(factureService.findActive());
    }

    //--------------------------une facture par son id-----------------------------------------
    @GetMapping(value = "/{id}")
    public ResponseEntity<FactureDto> Facture(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(factureService.findOne(id));
    }

    // -------------------desable une facture par son id---------------------------------------------
    @GetMapping(value = "/desable/{id}")
    public ResponseEntity<FactureDto> desablefactureParId(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(factureService.save(id));
    }
}
