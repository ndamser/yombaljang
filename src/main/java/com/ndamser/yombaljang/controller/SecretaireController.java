package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.SecretaireDto;
import com.ndamser.yombaljang.service.SecretaireService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/secretaires")
@AllArgsConstructor
@Slf4j
public class SecretaireController {

    private final SecretaireService secretaireService;

    // -------------------sauvgarder un secretaire---------------------------------------------
    @PostMapping(value = "/")
    public ResponseEntity<SecretaireDto> createSecretaire(@RequestBody SecretaireDto secretaireDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(secretaireService.saveSecretaire(secretaireDto));
    }

    // -------------------get secretaire---------------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<SecretaireDto>> getScretaires() {
        return ResponseEntity.status(HttpStatus.OK).body(secretaireService.findActive());
    }

    // -------------------desable Secretaire par son id---------------------------------------------
    @GetMapping(value = "/{id}/desable")
    public ResponseEntity<SecretaireDto> desableSecretaireParId(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(secretaireService.save(id));

    }

    //---------------------------------get per id-------------------------------------------
    //secretaire
    @GetMapping(value = "/{id}")
    public ResponseEntity<SecretaireDto> getSecretaireParId(@PathVariable long id) {
        return ResponseEntity.status(HttpStatus.OK).body(secretaireService.findOne(id));

    }

}
