package com.ndamser.yombaljang.controller;


import com.ndamser.yombaljang.dto.AideDto;
import com.ndamser.yombaljang.service.AideService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/aides")
@AllArgsConstructor
@Slf4j
public class AideController {

    private final AideService aideService;

    // -------------------sauvgarder un aide---------------------------------------------
    @PostMapping(value = "/")
    public ResponseEntity<AideDto> createAide(@RequestBody AideDto aideDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(aideService.saveAide(aideDto));
    }

    // -------------------get aides--------------------------------------------
    @GetMapping(value = "/")
    public ResponseEntity<List<AideDto>> getAides() {
        return ResponseEntity.status(HttpStatus.OK).body(aideService.findActive());
    }

    // -------------------desable aide par son id---------------------------------------------
    @GetMapping(value = "/{id}/desable")
    public ResponseEntity<AideDto> desableAideParId(@PathVariable("id") long id) {
        return ResponseEntity.status(HttpStatus.OK).body(aideService.save(id));
    }
}
