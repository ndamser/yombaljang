package com.ndamser.yombaljang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class YombaljangApplication {

	public static void main(String[] args) {
		SpringApplication.run(YombaljangApplication.class, args);
	}

}
