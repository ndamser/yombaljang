package com.ndamser.yombaljang.service;

import com.ndamser.yombaljang.dto.DevoirDto;
import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Devoir;
import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Matiere;
import com.ndamser.yombaljang.repository.ClasseRepository;
import com.ndamser.yombaljang.repository.DevoirRepository;
import com.ndamser.yombaljang.repository.EleveRepository;
import com.ndamser.yombaljang.repository.MatiereRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;


@Service
@AllArgsConstructor
@Slf4j
public class DevoirService {

    private final DevoirRepository devoirRepository;
    private final ClasseRepository classeRepository;
    private final MatiereRepository matiereRepository;
    private final EleveRepository eleveRepository;

    private DevoirDto mapToDto(Devoir devoir) {
        return DevoirDto.builder().classe(devoir.getClasse())
                .dateDevoir(devoir.getDateDevoir())
                .idDevoir(devoir.getIdDevoir())
                .matiere(devoir.getMatiere())
                .tache(devoir.getTache())
                .valide(devoir.isValide())
                .build();
    }

    private Devoir mapContactDto(DevoirDto devoirDto) {
        return Devoir.builder().classe(devoirDto.getClasse())
                .dateDevoir(devoirDto.getDateDevoir())
                .idDevoir(devoirDto.getIdDevoir())
                .matiere(devoirDto.getMatiere())
                .tache(devoirDto.getTache())
                .valide(devoirDto.isValide())
                .build();
    }

    public DevoirDto save(DevoirDto devoirDto, String matiere, String classe) {

        devoirDto.setMatiere(matiereRepository.findByNom(matiere));
        devoirDto.setClasse(classeRepository.findByNom(classe));
        Devoir devoirSave = devoirRepository.save(mapContactDto(devoirDto));
        devoirDto.setIdDevoir(devoirSave.getIdDevoir());
        return devoirDto;
    }


    public DevoirDto update(DevoirDto devoirDto) {

        Devoir devoiru = devoirRepository.getById(devoirDto.getIdDevoir());
        devoiru.setIdDevoir(devoirDto.getIdDevoir());
        devoiru.setClasse(devoirDto.getClasse());
        devoiru.setDateDevoir(devoirDto.getDateDevoir());
        devoiru.setTache(devoirDto.getTache());
        devoiru.setMatiere(devoirDto.getMatiere());

        devoirRepository.save(mapContactDto(devoirDto));

        return devoirDto;

    }

    public List<DevoirDto> findDevoirsParMatiere(String nomMatiere) {

        Matiere matiere = matiereRepository.findByNom(nomMatiere);

        return devoirRepository.findDevoirsParMatiere(matiere)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public List<DevoirDto> findDevoirsParEleve(String matricule) {

        Eleve eleve = eleveRepository.findByMatricule(matricule);
        Classe classe = eleve.getClasse();
        return devoirRepository.findDevoirsByClasse(classe, true)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public DevoirDto findOne(Long idDevoir) {
        return mapToDto(devoirRepository.getById(idDevoir));
    }

    public void delete(Long id) {
        devoirRepository.deleteById(id);
    }
}
