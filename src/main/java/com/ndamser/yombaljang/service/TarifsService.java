package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.TarifDto;
import com.ndamser.yombaljang.model.Tarif;
import com.ndamser.yombaljang.repository.TarifsRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class TarifsService {

    private final TarifsRepository tarifsRepository;

    private TarifDto mapToDto(Tarif tarif) {
        return TarifDto.builder().gardes(tarif.getGardes())
                .idTarif(tarif.getIdTarif())
                .inscriptions(tarif.getInscriptions())
                .montant(tarif.getMontant())
                .nomTarif(tarif.getNomTarif())
                .build();
    }

    private Tarif mapTarifDto(TarifDto tarifDto) {
        return Tarif.builder().gardes(tarifDto.getGardes())
                .idTarif(tarifDto.getIdTarif())
                .inscriptions(tarifDto.getInscriptions())
                .montant(tarifDto.getMontant())
                .nomTarif(tarifDto.getNomTarif())
                .build();
    }

    public TarifDto findByService(String service) {
        Tarif tarif = tarifsRepository.findParservice(service);
        return mapToDto(tarif);
    }
}
