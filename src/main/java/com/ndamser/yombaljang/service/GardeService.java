package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.GardeDto;
import com.ndamser.yombaljang.dto.ProfesseurDto;
import com.ndamser.yombaljang.model.Garde;
import com.ndamser.yombaljang.model.Professeur;
import com.ndamser.yombaljang.repository.AideRepository;
import com.ndamser.yombaljang.repository.GardeRepository;
import com.ndamser.yombaljang.repository.ProfesseurRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class GardeService {

    private final GardeRepository gardeRepository;
    private final ProfesseurRepository professeurRepository;
    private final AideRepository aideRepository;

    private GardeDto mapToDto(Garde garde) {
        return GardeDto.builder().active(garde.isActive())
                .aide(garde.getAide())
                .dateDebut(garde.getDateDebut())
                .dateFin(garde.getDateFin())
                .idGarde(garde.getIdGarde())
                .professeur(garde.getProfesseur())
                .tarif(garde.getTarif())
                .build();
    }

    private ProfesseurDto mapToDto(Professeur professeur) {
        return ProfesseurDto.builder().cin(professeur.getCin())
                .dateDebutTravail(professeur.getDateDebutTravail())
                .cahiertextes(professeur.getCahiertextes())
                .diplome(professeur.getDiplome())
                .build();
    }

    private Garde mapGardeDto(GardeDto gardeDto) {
        return Garde.builder().active(gardeDto.isActive())
                .aide(gardeDto.getAide())
                .dateDebut(gardeDto.getDateDebut())
                .dateFin(gardeDto.getDateFin())
                .idGarde(gardeDto.getIdGarde())
                .professeur(gardeDto.getProfesseur())
                .tarif(gardeDto.getTarif())
                .build();
    }

    public GardeDto saveGarde(GardeDto gardeDto, String cin, String matriculeAide) {
        gardeDto.setProfesseur(professeurRepository.findByCin(cin));
        gardeDto.setAide(aideRepository.findByCin(matriculeAide));

        Garde gardeSave = gardeRepository.save(mapGardeDto(gardeDto));
        gardeDto.setIdGarde(gardeSave.getIdGarde());
        return gardeDto;
    }

    public List<GardeDto> findActive() {
        return gardeRepository.findActive(true)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public GardeDto save(long id) {

        Garde garde = gardeRepository.findById(id).get();
        garde.setActive(false);
        return mapToDto(garde);
    }

    public GardeDto findOne(long id) {
        Garde garde = gardeRepository.findById(id).get();
        return mapToDto(garde);
    }

    public ProfesseurDto findProf(long id) {
        Garde g = gardeRepository.findById(id).get();
        return mapToDto(g.getProfesseur());

    }
}
