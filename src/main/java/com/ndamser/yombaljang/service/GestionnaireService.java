package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.GestionnaireDto;
import com.ndamser.yombaljang.model.Gestionnaire;
import com.ndamser.yombaljang.repository.GestionnaireRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class GestionnaireService {

    private final GestionnaireRepository gestionnaireRepository;

    private GestionnaireDto mapToDto(Gestionnaire gestionnaire) {
        return GestionnaireDto.builder().cin(gestionnaire.getCin())
                .dateDebutTravail(gestionnaire.getDateDebutTravail())
                .build();
    }

    private Gestionnaire mapGestionnaireDto(GestionnaireDto gestionnaireDto) {
        return Gestionnaire.builder().cin(gestionnaireDto.getCin())
                .dateDebutTravail(gestionnaireDto.getDateDebutTravail())
                .build();
    }

    public GestionnaireDto saveGestioonaire(GestionnaireDto gestionnaireDto) {
        Gestionnaire gestionnaire = gestionnaireRepository.save(mapGestionnaireDto(gestionnaireDto));
        gestionnaireDto.setIdPersonne(gestionnaire.getIdPersonne());

        return gestionnaireDto;
    }

    public List<GestionnaireDto> getAll() {
        return gestionnaireRepository.findActive(true)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public GestionnaireDto save(long id) {

        Gestionnaire gestionnaire = gestionnaireRepository.findById(id).get();
        gestionnaire.setActive(false);

        return mapToDto(gestionnaire);

    }
}
