package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.FemmeDeMenageDto;
import com.ndamser.yombaljang.model.FemmeDeMenage;
import com.ndamser.yombaljang.repository.FemmeDeMenageRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class FemmeDeMenageService {
    private final FemmeDeMenageRepository femmeDeMenageRepository;

    private FemmeDeMenageDto mapToDto(FemmeDeMenage femmeDeMenage) {
        return FemmeDeMenageDto.builder().cin(femmeDeMenage.getCin())
                .dateDebutTravail(femmeDeMenage.getDateDebutTravail())
                .build();
    }

    private FemmeDeMenage mapFemmeDeMenageDto(FemmeDeMenageDto femmeDeMenageDto) {
        return FemmeDeMenage.builder().cin(femmeDeMenageDto.getCin())
                .dateDebutTravail(femmeDeMenageDto.getDateDebutTravail())
                .build();
    }

    public FemmeDeMenageDto saveFM(FemmeDeMenageDto femmeDeMenageDto) {
        FemmeDeMenage femmeDeMenageSave = femmeDeMenageRepository.save(mapFemmeDeMenageDto((femmeDeMenageDto)));
        femmeDeMenageDto.setIdPersonne(femmeDeMenageSave.getIdPersonne());
        return femmeDeMenageDto;
    }

    public List<FemmeDeMenageDto> findActive() {
        return femmeDeMenageRepository.findActive(true)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }


    public FemmeDeMenageDto save(long id) {
        FemmeDeMenage femmeDeMenage = femmeDeMenageRepository.findById(id).get();
        femmeDeMenage.setActive(false);
        return mapToDto(femmeDeMenage);
    }
}
