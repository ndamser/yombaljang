package com.ndamser.yombaljang.service;

import com.ndamser.yombaljang.dto.EleveDto;
import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Inscription;
import com.ndamser.yombaljang.model.Note;
import com.ndamser.yombaljang.repository.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class EleveService {

    private final EleveRepository eleveRepository;

    private final ClasseRepository classeRepository;

    private final NiveauRepository niveauRepository;
    private final ResponsableRepository responsableRepository;
    private final InscriptionRepository inscriptionRepository;
    private final NoteRepository noteRepository;


    private EleveDto mapToDto(Eleve eleve) {
        return EleveDto.builder().classe(eleve.getClasse())
                .absences(eleve.getAbsences())
                .inscriptions(eleve.getInscriptions())
                .matricule(eleve.getMatricule())
                .niveau(eleve.getNiveau())
                .notes(eleve.getNotes())
                .responsables(eleve.getResponsables())
                .typePaiement(eleve.getTypePaiement())
                .build();
    }

    private Eleve mapEleveDto(EleveDto eleveDto) {
        return Eleve.builder().classe(eleveDto.getClasse())
                .absences(eleveDto.getAbsences())
                .inscriptions(eleveDto.getInscriptions())
                .matricule(eleveDto.getMatricule())
                .niveau(eleveDto.getNiveau())
                .notes(eleveDto.getNotes())
                .responsables(eleveDto.getResponsables())
                .typePaiement(eleveDto.getTypePaiement())
                .build();
    }


    public Eleve findByMatricule(String matricule) {
        Eleve eleve = eleveRepository.findByMatricule(matricule);
        return eleve;
    }

    public List<EleveDto> findActive() {
        return eleveRepository.findActive(true)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public List<EleveDto> findElevesParClasse(String nomClasse) {
        Classe classe = classeRepository.findByNom(nomClasse);
        return eleveRepository.findElevesParClasse(classe)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }


    public EleveDto saveEleve(EleveDto eleveDto) {

        Eleve saveEleve = eleveRepository.save(mapEleveDto(eleveDto));
        eleveDto.setIdPersonne(saveEleve.getIdPersonne());
        return eleveDto;
    }


    public EleveDto save(EleveDto eleveDto, String cin, Long idNiveau, String cin2) {

        eleveDto.setNiveau(niveauRepository.getById(idNiveau));
        eleveDto.setActive(true);
        eleveDto.setMatricule(generateId(eleveDto.getNom()));
        eleveRepository.save(mapEleveDto(eleveDto));

        eleveDto.getResponsables().add(responsableRepository.findParCin(cin));
        eleveDto.getResponsables().add(responsableRepository.findParCin(cin2));

        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        Inscription inscription = new Inscription();
        inscription.setDateInscription(date);
        inscription.setEleve(mapEleveDto(eleveDto));
        inscription.setValide(false);
        inscription.setCurrent(true);
        inscriptionRepository.save(inscription);

        Eleve eleve = eleveRepository.save(mapEleveDto(eleveDto));

        eleveDto.setIdPersonne(eleve.getIdPersonne());
        return eleveDto;

    }

    //generate the matricule
    public String generateId(String nom) {
        long randomId = 0;
        Random rand = new Random();
        String d = nom.substring(0, 3);
        randomId = rand.nextLong() / 1000000000;
        randomId = randomId / 100000;

        if (randomId < 0) {
            randomId = -randomId;
        }

        return d + randomId;
    }

    public EleveDto findByemail(String email) {
        return mapToDto(eleveRepository.findParEmail(email));
    }

    public EleveDto findOne(long id) {
        return mapToDto(eleveRepository.findById(id).get());
    }

    public EleveDto desableById(long id) {
        Eleve eleve = eleveRepository.findById(id).get();
        eleve.setActive(false);
        return mapToDto(eleve);

    }

    public EleveDto findEleve(Long idNote) {
        Note note = noteRepository.findById(idNote).get();
        Eleve eleve = note.getEleve();
        return mapToDto(eleve);
    }
}
