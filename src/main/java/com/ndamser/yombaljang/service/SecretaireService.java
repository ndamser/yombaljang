package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.SecretaireDto;
import com.ndamser.yombaljang.model.Secretaire;
import com.ndamser.yombaljang.repository.SecretaireRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class SecretaireService {

    private final SecretaireRepository secretaireRepository;

    private SecretaireDto mapToDto(Secretaire secretaire) {
        return SecretaireDto.builder().cin(secretaire.getCin())
                .dateDebutTravail(secretaire.getDateDebutTravail())
                .build();
    }

    private Secretaire mapSecretaireDto(SecretaireDto secretaireDto) {
        return Secretaire.builder().cin(secretaireDto.getCin())
                .dateDebutTravail(secretaireDto.getDateDebutTravail())
                .build();
    }

    public SecretaireDto saveSecretaire(SecretaireDto secretaireDto) {
        Secretaire secretaireSave = secretaireRepository.save(mapSecretaireDto(secretaireDto));
        secretaireDto.setIdPersonne(secretaireSave.getIdPersonne());
        return secretaireDto;
    }

    public List<SecretaireDto> findActive() {
        return secretaireRepository.findActive(true)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public SecretaireDto save(long id) {
        Secretaire secretaire = secretaireRepository.findById(id).get();
        secretaire.setActive(false);
        return mapToDto(secretaire);
    }

    public SecretaireDto findOne(long id) {
        return mapToDto(secretaireRepository.findById(id).get());
    }
}
