package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.ResponsableDto;
import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Responsable;
import com.ndamser.yombaljang.repository.EleveRepository;
import com.ndamser.yombaljang.repository.ResponsableRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class ResponsableService {

    private final ResponsableRepository responsableRepository;
    private final EleveRepository eleveRepository;

    private ResponsableDto mapToDto(Responsable responsable) {
        return ResponsableDto.builder().cin(responsable.getCin())
                .eleves(responsable.getEleves())
                .lienParente(responsable.getLienParente())
                .build();
    }

    private Responsable mapResponsableDto(ResponsableDto responsableDto) {
        return Responsable.builder().cin(responsableDto.getCin())
                .eleves(responsableDto.getEleves())
                .lienParente(responsableDto.getLienParente())
                .build();
    }

    public ResponsableDto findOne(long id) {
        Responsable responsable = responsableRepository.getById(id);
        return mapToDto(responsable);
    }

    public ResponsableDto saveRespo(ResponsableDto responsableDto) {
        responsableDto.setActive(true);
        Responsable responsableSave = responsableRepository.save(mapResponsableDto(responsableDto));

        responsableDto.setIdPersonne(responsableSave.getIdPersonne());
        return responsableDto;
    }

    public ResponsableDto findParCin(String cin) {
        Responsable responsable = responsableRepository.findParCin(cin);
        return mapToDto(responsable);
    }

    public List<ResponsableDto> findAll() {
        return responsableRepository.findAll()
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public ResponsableDto update(ResponsableDto responsableDto) {
        Set<Eleve> eleves = new HashSet<Eleve>(0);
        Eleve eleve = eleveRepository.findById((long) 35).get();
        eleves.add(eleve);
        responsableDto.setEleves(eleves);

        Responsable responsable = responsableRepository.save(mapResponsableDto(responsableDto));
        responsableDto.setIdPersonne(responsable.getIdPersonne());
        return responsableDto;
    }
}
