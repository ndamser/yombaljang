package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.FournisseurDto;
import com.ndamser.yombaljang.model.Contact;
import com.ndamser.yombaljang.model.Fournisseur;
import com.ndamser.yombaljang.repository.ContactRepository;
import com.ndamser.yombaljang.repository.FactureRepository;
import com.ndamser.yombaljang.repository.FournisseurRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class FournisseurService {

    private final FournisseurRepository fournisseurRepository;
    private final ContactRepository contactRepository;
    private final FactureRepository factureRepository;

    private FournisseurDto mapToDto(Fournisseur fournisseur) {
        return FournisseurDto.builder().active(fournisseur.isActive())
                .adresse(fournisseur.getAdresse())
                .contact(fournisseur.getContact())
                .email(fournisseur.getEmail())
                .idFournisseur(fournisseur.getIdFournisseur())
                .nom(fournisseur.getNom())
                .telephone(fournisseur.getTelephone())
                .ville(fournisseur.getVille())
                .build();
    }

    private Fournisseur mapFournisseurDto(FournisseurDto fournisseurDto) {
        return Fournisseur.builder().active(fournisseurDto.isActive())
                .adresse(fournisseurDto.getAdresse())
                .contact(fournisseurDto.getContact())
                .email(fournisseurDto.getEmail())
                .idFournisseur(fournisseurDto.getIdFournisseur())
                .nom(fournisseurDto.getNom())
                .telephone(fournisseurDto.getTelephone())
                .ville(fournisseurDto.getVille())
                .build();
    }

    public List<FournisseurDto> findActive() {
        return fournisseurRepository.findActive(true)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public FournisseurDto findOne(Long id) {
        return mapToDto(fournisseurRepository.findById(id).get());
    }

    public FournisseurDto saveFournisseur(FournisseurDto fournisseurDto, String cin) {
        Contact contact = contactRepository.findByCin(cin);
        fournisseurDto.setContact(contact);

        Fournisseur fournisseursave = fournisseurRepository.save(mapFournisseurDto(fournisseurDto));
        fournisseurDto.setIdFournisseur(fournisseursave.getIdFournisseur());
        return fournisseurDto;

    }

    public FournisseurDto save(Long id) {
        FournisseurDto fournisseurDto = findOne(id);
        fournisseurDto.setActive(false);
        return fournisseurDto;

    }

    public FournisseurDto findFournisseurFromFacture(Long id) {
        Fournisseur fournisseur = factureRepository.getById(id).getFournisseur();
        return mapToDto(fournisseur);
    }
}
