package com.ndamser.yombaljang.service;

import com.ndamser.yombaljang.dto.VehiculeDto;
import com.ndamser.yombaljang.model.Vehicule;
import com.ndamser.yombaljang.repository.VehiculeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class VehiculeService {

    private final VehiculeRepository vehiculeRepository;

    @Transactional
    public VehiculeDto save(VehiculeDto vehiculeDto) {
        vehiculeDto.setActive(true);
        Vehicule save = vehiculeRepository.save(mapVehiculeDto(vehiculeDto));
        vehiculeDto.setIdVehicule(save.getIdVehicule());
        return vehiculeDto;
    }

    @Transactional(readOnly = true)
    public List<VehiculeDto> getAll() {
        return vehiculeRepository.findAll()
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    private VehiculeDto mapToDto(Vehicule vehicule) {
        return VehiculeDto.builder().active(vehicule.isActive())
                .dateDachat(vehicule.getDateDachat())
                .idVehicule(vehicule.getIdVehicule())
                .marque(vehicule.getMarque())
                .matricule(vehicule.getMatricule())
                .modele(vehicule.getModele())
                .build();
    }

    private Vehicule mapVehiculeDto(VehiculeDto vehiculeDto) {
        return Vehicule.builder().active(vehiculeDto.isActive())
                .dateDachat(vehiculeDto.getDateDachat())
                .idVehicule(vehiculeDto.getIdVehicule())
                .marque(vehiculeDto.getMarque())
                .matricule(vehiculeDto.getMatricule())
                .modele(vehiculeDto.getModele())
                .build();
    }
}
