package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.CahierTexteDto;
import com.ndamser.yombaljang.model.CahierTexte;
import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Matiere;
import com.ndamser.yombaljang.repository.CahierTexteRepository;
import com.ndamser.yombaljang.repository.ClasseRepository;
import com.ndamser.yombaljang.repository.MatiereRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class CahierDeTexteService {

    private final CahierTexteRepository cahierTexteRepository;
    private final MatiereRepository matiereRepository;
    private final ClasseRepository classeRepository;

    private CahierTexteDto mapToDto(CahierTexte cahierTexte) {
        return CahierTexteDto.builder().date(cahierTexte.getDate())
                .classe(cahierTexte.getClasse())
                .idCahierTexte(cahierTexte.getIdCahierTexte())
                .matiere(cahierTexte.getMatiere())
                .professeur(cahierTexte.getProfesseur())
                .tache(cahierTexte.getTache())
                .build();
    }

    private CahierTexte mapCahierTexteDto(CahierTexteDto cahierTexteDto) {
        return CahierTexte.builder().classe(cahierTexteDto.getClasse())
                .date(cahierTexteDto.getDate())
                .idCahierTexte(cahierTexteDto.getIdCahierTexte())
                .matiere(cahierTexteDto.getMatiere())
                .professeur(cahierTexteDto.getProfesseur())
                .tache(cahierTexteDto.getTache())
                .build();
    }

    public CahierTexteDto saveCahierText(CahierTexteDto cahierTexteDto, String matiere, String classe) {
        cahierTexteDto.setMatiere(matiereRepository.findByNom(matiere));
        cahierTexteDto.setClasse(classeRepository.findByNom(classe));
        CahierTexte cahierTexteSave = cahierTexteRepository.save(mapCahierTexteDto(cahierTexteDto));
        cahierTexteDto.setIdCahierTexte(cahierTexteSave.getIdCahierTexte());
        return cahierTexteDto;
    }

    public CahierTexteDto findCahierParClasse(String nomClasse) {
        Classe classe = classeRepository.findByNom(nomClasse);
        CahierTexte cahierTexte = cahierTexteRepository.findCahierParClasse(classe);
        return mapToDto(cahierTexte);
    }

    public List<CahierTexteDto> findCahierParMatiere(String nomMatiere) {
        Matiere matiere = matiereRepository.findByNom(nomMatiere);

        return cahierTexteRepository.findCahierParMatiere(matiere)
                .stream()
                .map(this::mapToDto)
                .collect(toList());

    }

    public List<CahierTexteDto> findAll() {
        return cahierTexteRepository.findAll()
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public CahierTexteDto findOne(Long idCahierTexte) {
        return mapToDto(cahierTexteRepository.getById(idCahierTexte));
    }

    public void delele(long id) {
        cahierTexteRepository.deleteById(id);
    }
}
