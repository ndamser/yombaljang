package com.ndamser.yombaljang.service;

import com.ndamser.yombaljang.dto.AbsenceDto;
import com.ndamser.yombaljang.model.Absence;
import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.repository.AbsenceRepository;
import com.ndamser.yombaljang.repository.ClasseRepository;
import com.ndamser.yombaljang.repository.EleveRepository;
import com.ndamser.yombaljang.repository.MatiereRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class AbsenceService {


    private final AbsenceRepository absenceRepository;
    private final EleveRepository eleveRepository;
    private final MatiereRepository matiereRepository;
    private final ClasseRepository classeRepository;

    @Autowired
    private EleveService eleveService;


    public AbsenceDto saveAbsence(AbsenceDto absenceDto, String matriculeEleve, String matiere) {
        absenceDto.setJustifie(false);
        absenceDto.setEleve(eleveRepository.findByMatricule(matriculeEleve));
        absenceDto.setMatiere(matiereRepository.findByNom(matiere));
        //TODO: Enventualité d'envoie de mail à voir avec equipe
        Absence save = absenceRepository.save(mapAbsenceDto(absenceDto));
        absenceDto.setIdAbsence(save.getIdAbsence());
        return absenceDto;

    }

    private AbsenceDto mapToDto(Absence absence) {
        return AbsenceDto.builder().dateAbsence(absence.getDateAbsence())
                .eleve(absence.getEleve())
                .idAbsence(absence.getIdAbsence())
                .justifie(absence.isJustifie())
                .matiere(absence.getMatiere())
                .build();
    }

    private Absence mapAbsenceDto(AbsenceDto absenceDto) {
        return Absence.builder().dateAbsence(absenceDto.getDateAbsence())
                .eleve(absenceDto.getEleve())
                .idAbsence(absenceDto.getIdAbsence())
                .justifie(absenceDto.isJustifie())
                .matiere(absenceDto.getMatiere())
                .build();
    }

    public Absence justifierAbsaenceById(Long id) {
        Absence absence = absenceRepository.getById(id);
        absence.setJustifie(true);
        absenceRepository.save(absence);
        return absence;

    }

    public List<AbsenceDto> getabsenceByEleve(String matricule) {

        Eleve eleve = eleveService.findByMatricule(matricule);
        List<Absence> absences = absenceRepository.findParEleve(eleve);

        String nomMatiere;

        for (int i = 0; i < absences.size(); i++) {
            nomMatiere = absences.get(i).getMatiere().getNomMatiere();
            absences.get(i).setM(nomMatiere);

        }

        return absenceRepository.findParEleveMatricule(matricule)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public List<AbsenceDto> findParClasse(String nomclasse) {
        Classe classe = classeRepository.findByNom(nomclasse);
        return absenceRepository.findParClasse(classe)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public void deleteAbsenceById(Long id) {
        absenceRepository.deleteById(id);
    }
}
