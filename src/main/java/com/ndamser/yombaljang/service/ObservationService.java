package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.ObservationDto;
import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Observation;
import com.ndamser.yombaljang.repository.EleveRepository;
import com.ndamser.yombaljang.repository.MatiereRepository;
import com.ndamser.yombaljang.repository.ObservationRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class ObservationService {

    private final ObservationRepository observationRepository;
    private final EleveRepository eleveRepository;
    private final MatiereRepository matiereRepository;


    private ObservationDto mapToDto(Observation observation) {
        return ObservationDto.builder().date(observation.getDate())
                .eleve(observation.getEleve())
                .idObservation(observation.getIdObservation())
                .matiere(observation.getMatiere())
                .remarque(observation.getRemarque())
                .build();
    }

    private Observation mapObservationDto(ObservationDto observationDto) {
        return Observation.builder().date(observationDto.getDate())
                .eleve(observationDto.getEleve())
                .idObservation(observationDto.getIdObservation())
                .matiere(observationDto.getMatiere())
                .remarque(observationDto.getRemarque())
                .build();
    }

    public ObservationDto saveObservation(ObservationDto observationDto, String matiere, String eleve) {
        observationDto.setEleve(eleveRepository.findByMatricule(eleve));
        observationDto.setMatiere(matiereRepository.findByNom(matiere));

        Observation observation = observationRepository.save(mapObservationDto(observationDto));
        observationDto.setIdObservation(observation.getIdObservation());

        return observationDto;
    }

    public List<ObservationDto> findByEleve(String matricule) {
        Eleve eleve = eleveRepository.findByMatricule(matricule);
        return observationRepository.findByEleve(eleve)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public ObservationDto findOne(Long idObservation) {
        Observation observation = observationRepository.findById(idObservation).get();
        return mapToDto(observation);
    }

    public void delete(Long id) {
        observationRepository.deleteById(id);
    }
}
