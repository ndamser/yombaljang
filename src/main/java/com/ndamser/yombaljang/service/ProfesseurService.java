package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.ProfesseurDto;
import com.ndamser.yombaljang.model.Professeur;
import com.ndamser.yombaljang.repository.ProfesseurRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class ProfesseurService {

    private final ProfesseurRepository professeurRepository;

    private ProfesseurDto mapToDto(Professeur professeur) {
        return ProfesseurDto.builder().cin(professeur.getCin())
                .dateDebutTravail(professeur.getDateDebutTravail())
                .cahiertextes(professeur.getCahiertextes())
                .diplome(professeur.getDiplome())
                .build();
    }

    private Professeur mapProfesseurDto(ProfesseurDto professeurDto) {
        return Professeur.builder().cin(professeurDto.getCin())
                .dateDebutTravail(professeurDto.getDateDebutTravail())
                .cahiertextes(professeurDto.getCahiertextes())
                .diplome(professeurDto.getDiplome())
                .build();
    }

    public ProfesseurDto saveProfesseur(ProfesseurDto professeurDto) {
        Professeur professeur = professeurRepository.save(mapProfesseurDto(professeurDto));
        professeurDto.setIdPersonne(professeur.getIdPersonne());
        return professeurDto;
    }

    public List<ProfesseurDto> findActive() {
        return professeurRepository.findActive(true)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public ProfesseurDto save(long id) {
        Professeur professeur = professeurRepository.findById(id).get();
        professeur.setActive(false);
        return mapToDto(professeur);
    }
}
