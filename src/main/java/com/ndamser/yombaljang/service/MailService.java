package com.ndamser.yombaljang.service;

import com.ndamser.yombaljang.exception.YombalJangException;
import com.ndamser.yombaljang.model.NotificationEmail;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class MailService {

    private final JavaMailSender mailSender;
    private final MailContentBuilder mailContentBuilder;

    @Async
    public void sendMail(NotificationEmail notificationEmail) {

        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom("NDAMSER@EMAIL.COM");
            messageHelper.setTo(notificationEmail.getRecipient());
            messageHelper.setSubject(notificationEmail.getSubject());
            messageHelper.setText(mailContentBuilder.build(notificationEmail.getBody()));
        };

        try {
            mailSender.send(messagePreparator);
            log.info("Mail d'activation envoyé avec succes à  !! "+notificationEmail.getRecipient());
           // System.out.println("Mail d'activation envoyé avec succes à  !! "+notificationEmail.getRecipient());
        }catch (MailException e) {
            throw new YombalJangException("Erreur lors de l'envoie du mail à "+notificationEmail.getRecipient());
        }

    }
}
