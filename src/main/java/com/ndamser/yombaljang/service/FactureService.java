package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.FactureDto;
import com.ndamser.yombaljang.model.Facture;
import com.ndamser.yombaljang.model.Fournisseur;
import com.ndamser.yombaljang.repository.FactureRepository;
import com.ndamser.yombaljang.repository.FournisseurRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class FactureService {

    private final FactureRepository factureRepository;
    private final FournisseurRepository fournisseurRepository;

    private FactureDto mapToDto(Facture facture) {
        return FactureDto.builder().active(facture.isActive())
                .date(facture.getDate())
                .fournisseur(facture.getFournisseur())
                .idFacture(facture.getIdFacture())
                .nombreProduit(facture.getNombreProduit())
                .numeroFacture(facture.getNumeroFacture())
                .prixHT(facture.getPrixHT())
                .produit(facture.getProduit())
                .totalHT(facture.getTotalHT())
                .build();
    }

    private Facture mapFactureDto(FactureDto factureDto) {
        return Facture.builder().active(factureDto.isActive())
                .date(factureDto.getDate())
                .fournisseur(factureDto.getFournisseur())
                .idFacture(factureDto.getIdFacture())
                .nombreProduit(factureDto.getNombreProduit())
                .numeroFacture(factureDto.getNumeroFacture())
                .prixHT(factureDto.getPrixHT())
                .produit(factureDto.getProduit())
                .totalHT(factureDto.getTotalHT())
                .build();
    }

    public FactureDto saveFacture(FactureDto factureDto, String nomFournisseur) {
        Fournisseur fournisseur = fournisseurRepository.findByNom(nomFournisseur);
        factureDto.setFournisseur(fournisseur);

        Facture factureSave = factureRepository.save(mapFactureDto(factureDto));
        factureDto.setIdFacture(factureSave.getIdFacture());
        return factureDto;

    }

    public List<FactureDto> findActive() {
        return factureRepository.findActive(true)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public FactureDto findOne(long id) {
        return mapToDto(factureRepository.findById(id).get());
    }

    public FactureDto save(long id) {
        Facture facture = factureRepository.findById(id).get();
        facture.setActive(false);
        Facture factureSave = factureRepository.save(facture);
        return mapToDto(factureSave);
    }
}
