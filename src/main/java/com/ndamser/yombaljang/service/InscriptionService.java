package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.InscriptionDto;
import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Inscription;
import com.ndamser.yombaljang.repository.EleveRepository;
import com.ndamser.yombaljang.repository.InscriptionRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class InscriptionService {

    private final InscriptionRepository inscriptionRepository;
    private final EleveRepository eleveRepository;

    private InscriptionDto mapToDto(Inscription inscription) {
        return InscriptionDto.builder().current(inscription.isCurrent())
                .dateInscription(inscription.getDateInscription())
                .eleve(inscription.getEleve())
                .idInscription(inscription.getIdInscription())
                .tarif(inscription.getTarif())
                .valide(inscription.isValide())
                .build();
    }

    private Inscription mapInscriptionDto(InscriptionDto inscriptionDto) {
        return Inscription.builder().current(inscriptionDto.isCurrent())
                .dateInscription(inscriptionDto.getDateInscription())
                .eleve(inscriptionDto.getEleve())
                .idInscription(inscriptionDto.getIdInscription())
                .tarif(inscriptionDto.getTarif())
                .valide(inscriptionDto.isValide())
                .build();
    }

    public InscriptionDto saveInscription(InscriptionDto inscriptionDto, String email) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        Eleve eleve = eleveRepository.findParEmail(email);
        inscriptionDto.setEleve(eleve);
        inscriptionDto.setDateInscription(date);

        Inscription inscriptionSave = inscriptionRepository.save(mapInscriptionDto(inscriptionDto));
        inscriptionDto.setIdInscription(inscriptionSave.getIdInscription());
        return inscriptionDto;
    }

    public List<InscriptionDto> findAll() {
        return inscriptionRepository.findAll()
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public InscriptionDto findCurrentInscription(String email) {

        Eleve eleve = eleveRepository.findParEmail(email);
        List<Inscription> ins = inscriptionRepository.getInscriptionsParEleve(eleve);
        for (int i = 0; i < ins.size(); i++) {
            if (ins.get(i).isCurrent())
                return mapToDto(ins.get(i));
        }
        return null;

    }
}
