package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.ContactDto;
import com.ndamser.yombaljang.model.Contact;
import com.ndamser.yombaljang.model.Fournisseur;
import com.ndamser.yombaljang.repository.ContactRepository;
import com.ndamser.yombaljang.repository.FournisseurRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class ContactService {

    private final ContactRepository contactRepository;
    @Autowired
    private final FournisseurRepository fournisseurRepository;

    private ContactDto mapToDto(Contact contact) {
        return ContactDto.builder().cin(contact.getCin())
                .fonction(contact.getFonction())
                .build();
    }

    private Contact mapContactDto(ContactDto contactDto) {
        return Contact.builder().cin(contactDto.getCin())
                .fonction(contactDto.getFonction())
                .build();
    }

    public ContactDto save(ContactDto contactDto) {
        Contact contactSave = contactRepository.save(mapContactDto(contactDto));
        contactDto.setCin(contactSave.getCin());
        return contactDto;
    }

    public ContactDto findContactFromFournisseur(Long id) {
        Fournisseur fournisseur = fournisseurRepository.findById(id).get();
        return mapToDto(fournisseur.getContact());
    }
}
