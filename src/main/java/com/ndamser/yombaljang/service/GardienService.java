package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.GardienDto;
import com.ndamser.yombaljang.model.Gardien;
import com.ndamser.yombaljang.repository.GardienRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class GardienService {

    private final GardienRepository gardienRepository;

    private GardienDto mapToDto(Gardien gardien) {
        return GardienDto.builder().cin(gardien.getCin())
                .dateDebutTravail(gardien.getDateDebutTravail())
                .build();
    }

    private Gardien mapGardienDto(GardienDto gardienDto) {
        return Gardien.builder().cin(gardienDto.getCin())
                .dateDebutTravail(gardienDto.getDateDebutTravail())
                .build();
    }

    public GardienDto saveGardien(GardienDto gardienDto) {
        Gardien gardienSave = gardienRepository.save(mapGardienDto(gardienDto));
        gardienDto.setIdPersonne(gardienSave.getIdPersonne());

        return gardienDto;
    }

    public List<GardienDto> findActive() {
        return gardienRepository.findActive(true)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public GardienDto save(long id) {
        Gardien gardien = gardienRepository.findById(id).get();
        gardien.setActive(false);
        return mapToDto(gardien);
    }
}
