package com.ndamser.yombaljang.service;

import com.ndamser.yombaljang.dto.ExamenDto;
import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Examen;
import com.ndamser.yombaljang.model.Matiere;
import com.ndamser.yombaljang.repository.ClasseRepository;
import com.ndamser.yombaljang.repository.EleveRepository;
import com.ndamser.yombaljang.repository.ExamenRepository;
import com.ndamser.yombaljang.repository.MatiereRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;


@Service
@AllArgsConstructor
@Slf4j
public class ExamenService {

    private final MatiereRepository matiereRepository;
    private final ClasseRepository classeRepository;
    private final ExamenRepository examenRepository;
    private final EleveRepository eleveRepository;

    private ExamenDto mapToDto(Examen examen) {
        return ExamenDto.builder().classe(examen.getClasse())
                .date(examen.getDate())
                .duree(examen.getDuree())
                .idExamen(examen.getIdExamen())
                .matiere(examen.getMatiere())
                .notes(examen.getNotes())
                .numExamen(examen.getNumExamen())
                .pourcentage(examen.getPourcentage())
                .valide(examen.isValide())
                .build();
    }

    private Examen mapExamenDto(ExamenDto examenDto) {
        return Examen.builder().classe(examenDto.getClasse())
                .date(examenDto.getDate())
                .duree(examenDto.getDuree())
                .idExamen(examenDto.getIdExamen())
                .matiere(examenDto.getMatiere())
                .notes(examenDto.getNotes())
                .numExamen(examenDto.getNumExamen())
                .pourcentage(examenDto.getPourcentage())
                .valide(examenDto.isValide())
                .build();
    }

    public ExamenDto saveExamen(ExamenDto examenDto, String matiere, String classe) {
        examenDto.setMatiere(matiereRepository.findByNom(matiere));
        examenDto.setClasse(classeRepository.findByNom(classe));

        Examen saveExamen = examenRepository.save(mapExamenDto(examenDto));
        examenDto.setIdExamen(saveExamen.getIdExamen());
        return examenDto;

    }

    public List<ExamenDto> findByEleve(String matricule) {

        Eleve eleve = eleveRepository.findByMatricule(matricule);
        Classe classe = eleve.getClasse();
        return examenRepository.findByClasse(classe)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public List<ExamenDto> findParMatiere(String nomMatiere) {
        Matiere matiere = matiereRepository.findByNom(nomMatiere);
        return examenRepository.findParMatiere(matiere)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public ExamenDto findOne(Long idExamen) {
        return mapToDto(examenRepository.findById(idExamen).get());
    }

    public void delete(Long id) {
        examenRepository.deleteById(id);
    }

    public List<ExamenDto> findByClasse(String nomClasse) {
        Classe classe = classeRepository.findByNom(nomClasse);
        return examenRepository.findByClasse(classe)
                .stream().map(this::mapToDto)
                .collect(toList());
    }
}
