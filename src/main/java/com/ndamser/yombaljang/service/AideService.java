package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.AideDto;
import com.ndamser.yombaljang.model.Aide;
import com.ndamser.yombaljang.repository.AideRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class AideService {

    private final AideRepository aideRepository;

    private AideDto mapToDto(Aide aide) {
        return AideDto.builder().cin(aide.getCin())
                .dateDebutTravail(aide.getDateDebutTravail())
                .gardes(aide.getGardes())
                .build();
    }

    private Aide mapAideDto(AideDto aideDto) {
        return Aide.builder().cin(aideDto.getCin())
                .dateDebutTravail(aideDto.getDateDebutTravail())
                .gardes(aideDto.getGardes())
                .build();
    }

    public AideDto saveAide(AideDto aideDto) {
        Aide aide = aideRepository.save(mapAideDto(aideDto));
        aideDto.setIdPersonne(aide.getIdPersonne());
        return aideDto;
    }

    public List<AideDto> findActive() {
        return aideRepository.findActive(true)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public AideDto save(long id) {

        Aide aide = aideRepository.findById(id).get();
        aide.setActive(false);

        return mapToDto(aide);
    }
}
