package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.NoteDto;
import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Matiere;
import com.ndamser.yombaljang.model.Note;
import com.ndamser.yombaljang.repository.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class NoteService {

    private final NoteRepository noteRepository;
    private final EleveRepository eleveRepository;
    private final MatiereRepository matiereRepository;
    private final ClasseRepository classeRepository;
    private final ExamenRepository examenRepository;

    private NoteDto mapToDto(Note note) {
        return NoteDto.builder().eleve(note.getEleve())
                .examen(note.getExamen())
                .idNote(note.getIdNote())
                .matiere(note.getMatiere())
                .noteExam(note.getNoteExam())
                .remarque(note.getRemarque())
                .build();
    }

    private Note mapNoteDto(NoteDto noteDto) {
        return Note.builder().eleve(noteDto.getEleve())
                .examen(noteDto.getExamen())
                .idNote(noteDto.getIdNote())
                .matiere(noteDto.getMatiere())
                .noteExam(noteDto.getNoteExam())
                .remarque(noteDto.getRemarque())
                .build();
    }

    public NoteDto saveNote(NoteDto noteDto, String matiere, String eleve) {

        noteDto.setEleve(eleveRepository.findParEmail(eleve));
        noteDto.setMatiere(matiereRepository.findByNom(matiere));

        Note note = noteRepository.save(mapNoteDto(noteDto));
        noteDto.setIdNote(note.getIdNote());
        return noteDto;
    }

    public List<NoteDto> findByClasseAndMatiere(String nomClasse, String nomMatiere) {


        Matiere matiere = matiereRepository.findByNom(nomMatiere);
        Eleve eleve = eleveRepository.findByMatricule(nomClasse);
        return noteRepository.findByEleveAndMatiere(eleve, matiere)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public List<NoteDto> findByEleveMatier(String matricule, String nomMatiere) {
        Matiere matiere = matiereRepository.findByNom(nomMatiere);
        Eleve eleve = eleveRepository.findByMatricule(matricule);
        return noteRepository.findByEleve(eleve, matiere)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public void delete(Long id) {
        noteRepository.deleteById(id);
    }

    public NoteDto findOne(Long idNote) {
        Note note = noteRepository.findById(idNote).get();
        return mapToDto(note);
    }
}
