package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.ChauffeurDto;
import com.ndamser.yombaljang.model.Chauffeur;
import com.ndamser.yombaljang.repository.ChauffeurRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class ChauffeurService {

    private final ChauffeurRepository chauffeurRepository;

    private ChauffeurDto mapToDto(Chauffeur chauffeur) {
        return ChauffeurDto.builder().cin(chauffeur.getCin())
                .dateDebutTravail(chauffeur.getDateDebutTravail())
                .build();
    }

    private Chauffeur mapChauffeurDto(ChauffeurDto chauffeurDto) {
        return Chauffeur.builder().cin(chauffeurDto.getCin())
                .dateDebutTravail(chauffeurDto.getDateDebutTravail())
                .build();
    }

    public ChauffeurDto saveChauffeur(ChauffeurDto chauffeurDto) {
        Chauffeur chauffeur = chauffeurRepository.save(mapChauffeurDto(chauffeurDto));
        chauffeurDto.setIdPersonne(chauffeur.getIdPersonne());
        return chauffeurDto;
    }

    public List<ChauffeurDto> findActive() {
        return chauffeurRepository.findActive(true)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public ChauffeurDto save(Long id) {
        Chauffeur chauffeur = chauffeurRepository.findById(id).get();
        chauffeur.setActive(false);
        return mapToDto(chauffeur);

    }
}
