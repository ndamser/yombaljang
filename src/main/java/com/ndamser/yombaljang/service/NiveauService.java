package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.NiveauDto;
import com.ndamser.yombaljang.model.Niveau;
import com.ndamser.yombaljang.repository.NiveauRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class NiveauService {

    private final NiveauRepository niveauRepository;

    private NiveauDto mapToDto(Niveau niveau) {
        return NiveauDto.builder().eleves(niveau.getEleves())
                .idNiveau(niveau.getIdNiveau())
                .niveau(niveau.getNiveau())
                .build();
    }

    private Niveau mapNiveauDto(NiveauDto niveauDto) {
        return Niveau.builder().eleves(niveauDto.getEleves())
                .idNiveau(niveauDto.getIdNiveau())
                .niveau(niveauDto.getNiveau())
                .build();
    }

    public List<NiveauDto> findAll() {
        return niveauRepository.findAll()
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }
}
