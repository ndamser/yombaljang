package com.ndamser.yombaljang.service;


import com.ndamser.yombaljang.dto.RetardDto;
import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Retard;
import com.ndamser.yombaljang.repository.ClasseRepository;
import com.ndamser.yombaljang.repository.EleveRepository;
import com.ndamser.yombaljang.repository.MatiereRepository;
import com.ndamser.yombaljang.repository.RetardRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
@Slf4j
public class RetardService {

    private final RetardRepository retardRepository;
    private final EleveRepository eleveRepository;
    private final MatiereRepository matiereRepository;
    private final ClasseRepository classeRepository;

    public RetardDto saveRetard(RetardDto retardDto, String matriculeEleve, String nomMatiere) {
        retardDto.setEleve(eleveRepository.findByMatricule(matriculeEleve));
        retardDto.setMatiere(matiereRepository.findByNom(nomMatiere));
        Retard retardSave = retardRepository.save(mapRetardDto(retardDto));
        retardDto.setIdRetard(retardSave.getIdRetard());
        return retardDto;
    }

    private RetardDto mapToDto(Retard retard) {
        return RetardDto.builder()
                .eleve(retard.getEleve())
                .idRetard(retard.getIdRetard())
                .matiere(retard.getMatiere())
                .build();
    }

    private Retard mapRetardDto(RetardDto retardDto) {
        return Retard.builder().eleve(retardDto.getEleve())
                .idRetard(retardDto.getIdRetard())
                .matiere(retardDto.getMatiere())
                .build();
    }

    public List<RetardDto> findParClasse(String nomclasse) {
        Classe classe = classeRepository.findByNom(nomclasse);
        return retardRepository.findParClasse(classe)
                .stream()
                .map(this::mapToDto)
                .collect(toList());
    }

    public void deleteRetardById(Long id) {
        retardRepository.deleteById(id);
    }
}
