package com.ndamser.yombaljang.exception;

public class YombalJangException extends RuntimeException {
    public YombalJangException(String exceptionMessage) {
        super(exceptionMessage);
    }
}
