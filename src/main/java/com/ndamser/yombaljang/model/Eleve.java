package com.ndamser.yombaljang.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@PrimaryKeyJoinColumn(name = "idPersonne")
public class Eleve extends Personne {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idClasse" )
    private Classe classe;
    @OneToMany(fetch = FetchType.LAZY ,mappedBy ="eleve")
    private Set<Inscription> inscriptions=new HashSet<Inscription>(0);
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idNiveau", nullable = false)
    private Niveau niveau;
    private int typePaiement;
    @JsonIgnore
    @ManyToMany()
    @JoinTable(name = "respo_eleve", joinColumns = @JoinColumn(name = "eleve", referencedColumnName = "idPersonne"),
            inverseJoinColumns = @JoinColumn(name = "respo", referencedColumnName = "idPersonne"))
    private Set<Responsable> responsables = new HashSet<Responsable>(0);
    private String matricule;
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "eleve")
    private Set<Absence> absences = new HashSet<Absence>(0);
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "eleve")
    private Set<Note> notes=new HashSet<Note>(0);
}
