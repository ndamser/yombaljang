package com.ndamser.yombaljang.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Tarif {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idTarif;
    private int montant;
    private String nomTarif;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tarif")
    private Set<Garde> gardes = new HashSet<Garde>(0);
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tarif")
    private Set<Inscription> inscriptions = new HashSet<Inscription>(0);
}
