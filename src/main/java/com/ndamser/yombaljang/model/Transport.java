package com.ndamser.yombaljang.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Transport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idTransport;
    @OneToOne(fetch = FetchType.LAZY)
    private Chauffeur chauffeur;
    @OneToOne(fetch = FetchType.LAZY)
    private Aide aide;
    private Date dateDebut;
    private Date dateFin;
    @OneToOne(fetch = FetchType.LAZY)
    private Vehicule vehicule;
    private boolean active;
}
