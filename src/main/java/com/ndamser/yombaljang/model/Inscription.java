package com.ndamser.yombaljang.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Inscription {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idInscription;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idTarif")
    private Tarif tarif;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DateInscription", length = 19)
    private Date dateInscription;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idPersonne")
    private Eleve eleve;
    private boolean valide;
    private boolean current;
}
