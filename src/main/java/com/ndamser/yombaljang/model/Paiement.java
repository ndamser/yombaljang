package com.ndamser.yombaljang.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Paiement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPaiement;
    private String typeService;
    private float montant;
    private String modePaiement;//cheque espece ,virement
    @OneToOne(fetch = FetchType.LAZY)
    private Inscription inscription;
    private String numCheque;
    private Date datePaiment;
    private String numRecu;
    private String mois;
    @OneToOne(fetch = FetchType.LAZY)
    private Eleve eleve;
}
