package com.ndamser.yombaljang.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@PrimaryKeyJoinColumn(name = "idPersonne")
public class Chauffeur extends Personne {

    private String cin;
    @Temporal(TemporalType.DATE)
    @Column(name = "DateDebutTravail", length = 10)
    private Date dateDebutTravail;
}
