package com.ndamser.yombaljang.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Classe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idClasse;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idNiveau")
    private Niveau niveau;
    private String nomClasse;
    private int nombreEleves;
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "classe")
    private Set<Eleve> eleves = new HashSet<Eleve>(0);
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "classe")
    private Set<Devoir> devoirs = new HashSet<Devoir>(0);
}
