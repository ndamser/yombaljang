package com.ndamser.yombaljang.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Examen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idExamen;
    @OneToOne(fetch = FetchType.LAZY)
    private Matiere matiere;
    @OneToOne(fetch = FetchType.LAZY)
    private Classe classe;
    private String numExamen;
    private Integer pourcentage;
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "examen")
    private Set<Note> notes = new HashSet<Note>(0);
    private boolean  valide;
    private Date date;
    private String duree;
}
