package com.ndamser.yombaljang.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CahierTexte {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCahierTexte;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idProfesseur")
    private Professeur professeur;
    private String date;
    private String tache;
    @OneToOne(fetch = FetchType.LAZY)
    private Matiere matiere;
    @OneToOne(fetch = FetchType.LAZY)
    private Classe classe;
}
