package com.ndamser.yombaljang.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Absence {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAbsence;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "Eleve_idEleve",  nullable = false)
    private Eleve eleve;
    @Temporal(TemporalType.DATE)
    @Column(name = "DateAbsence", length = 10)
    private Date dateAbsence;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idMatiere", referencedColumnName = "idMatiere", nullable = false)
    private Matiere matiere;
    private boolean justifie;
    private String m;


}
