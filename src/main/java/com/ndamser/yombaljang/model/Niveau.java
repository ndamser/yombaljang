package com.ndamser.yombaljang.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Niveau {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idNiveau;
    private String niveau;
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "niveau")
    private Set<Eleve> eleves = new HashSet<Eleve>(0);

}
