package com.ndamser.yombaljang.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy= InheritanceType.JOINED)
public abstract class Personne {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPersonne;
    private String sexe;
    private String nom;
    private String prenom;
    private Date dateDeNaissance;
    private String email;
    private String adresse;
    private String telephone;
    private String codePostale;
    private String ville;
    private String pays;
    private boolean active;
}
