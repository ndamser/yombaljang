package com.ndamser.yombaljang.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@PrimaryKeyJoinColumn(name = "idPersonne")
public class Professeur extends Personne {

    private String cin;
    private String diplome;
    @Temporal(TemporalType.DATE)
    @Column(name = "DateDebutTravail", length = 10)
    private Date dateDebutTravail;
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "professeur")
    private Set<CahierTexte> cahiertextes = new HashSet<CahierTexte>(0);
}
