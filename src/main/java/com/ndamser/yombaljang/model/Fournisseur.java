package com.ndamser.yombaljang.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Fournisseur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idFournisseur;
    private String adresse;
    private String email;
    private String ville;
    private String telephone;
    private String nom;
    private boolean active;
    @OneToOne(fetch = FetchType.LAZY)
    private Contact contact;
}
