package com.ndamser.yombaljang.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@PrimaryKeyJoinColumn(name = "idPersonne")
public class Responsable extends Personne {

    private String cin;
    private String lienParente;
    @JsonIgnore
    @ManyToMany(mappedBy = "responsables")
    private Set<Eleve> eleves = new HashSet<Eleve>(0);
}
