package com.ndamser.yombaljang.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Matiere {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idMatiere;
    private String nomMatiere;
    private String coeficient;
    private String nombreHeure;
    private String nombreExamen;
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "matiere")
    private Set<Absence> absences = new HashSet<Absence>(0);
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "matiere")
    private Set<Retard> retards = new HashSet<Retard>(0);
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "matiere")
    private Set<Devoir> devoirs = new HashSet<Devoir>(0);
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "matiere")
    private Set<Observation> observations = new HashSet<Observation>(0);
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "eleve")
    private Set<Note> notes = new HashSet<Note>(0);
}
