package com.ndamser.yombaljang.repository;

 import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Inscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InscriptionRepository extends JpaRepository<Inscription, Long> {
	
	@Query("select e from Inscription e where e.eleve=:x ")
	List<Inscription> getInscriptionsParEleve(@Param("x") Eleve eleve);
}
