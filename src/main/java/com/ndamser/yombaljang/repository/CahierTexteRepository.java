package com.ndamser.yombaljang.repository;

import com.ndamser.yombaljang.model.CahierTexte;
import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Matiere;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CahierTexteRepository extends JpaRepository<CahierTexte, Long> {

	@Query("select c from CahierTexte c where c.classe=:x ")
     CahierTexte findCahierParClasse(@Param("x") Classe classe);
	
	@Query("select c from CahierTexte c where c.matiere=:x ")
    List<CahierTexte> findCahierParMatiere(@Param("x") Matiere matiere);

}
