package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Classe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface ClasseRepository extends JpaRepository<Classe, Long> {

	@Query("select p from Classe p where p.nomClasse=:x ")
	Classe findByNom(@Param("x") String classe);

}
