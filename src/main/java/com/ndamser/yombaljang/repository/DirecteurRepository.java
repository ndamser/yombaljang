package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Directeur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DirecteurRepository extends CrudRepository<Directeur, Long> {

}

