package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Tarif;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface TarifsRepository extends JpaRepository<Tarif, Long> {
	 @Query("select p from Tarif p where p.nomTarif=:x ")
	 Tarif findParservice(@Param("x") String nomTarif);

}
