package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Facture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface FactureRepository extends JpaRepository<Facture, Long> {
	@Query("select e from Facture e where e.active=:x ")
	List<Facture> findActive(@Param("x") boolean active);

}
