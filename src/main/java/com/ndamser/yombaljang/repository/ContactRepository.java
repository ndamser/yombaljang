package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
	@Query("select a from Contact a where a.cin=:x ")
	Contact findByCin(@Param("x") String cin);

}
