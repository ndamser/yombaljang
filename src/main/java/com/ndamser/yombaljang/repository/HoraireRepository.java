package com.ndamser.yombaljang.repository;

import com.ndamser.yombaljang.model.Horaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface HoraireRepository extends JpaRepository<Horaire, Long> {

}
