package com.ndamser.yombaljang.repository;

import com.ndamser.yombaljang.model.Niveau;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface NiveauRepository extends JpaRepository<Niveau, Long> {

}

