package com.ndamser.yombaljang.repository;

import com.ndamser.yombaljang.model.FemmeDeMenage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface FemmeDeMenageRepository extends JpaRepository<FemmeDeMenage, Long>
{

	@Query("select e from FemmeDeMenage e where e.active=:x ")
    List<FemmeDeMenage> findActive(@Param("x") boolean active);
}
