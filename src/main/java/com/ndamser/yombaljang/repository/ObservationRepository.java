package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Observation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ObservationRepository extends JpaRepository<Observation, Long> {


	@Query("select o from Observation o where o.eleve=:x ")
    List<Observation>findByEleve(@Param("x") Eleve eleve);

}
