package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long> {
	
	@Query("select s from Service s where s.nomService=:x and s.eleve=:y ")
    Service findByEleve(@Param("x") String nomService, @Param("y") Eleve eleve);

	@Query("select s from Service s where s.eleve=:y ")
    List<Service>findServicesParEleve(@Param("y") Eleve eleve);


}
