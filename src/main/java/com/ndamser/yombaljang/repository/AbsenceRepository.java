package com.ndamser.yombaljang.repository;

import com.ndamser.yombaljang.model.Absence;
import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Eleve;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AbsenceRepository extends JpaRepository<Absence, Long> {

    @Query("select a from Absence a where a.eleve=:x ")
    List<Absence> findParEleve(@Param("x") Eleve eleve);

    @Query("select a from Absence a where a.eleve.matricule=:x ")
    List<Absence> findParEleveMatricule(@Param("x") String matricule);

    @Query("select a from Absence a where a.eleve.classe=:x ")
    List<Absence> findParClasse(@Param("x") Classe classe);

    List<Absence> findByEleve(Eleve eleve);




}
