package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Vehicule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface VehiculeRepository extends JpaRepository<Vehicule, Long> {
	
	 @Query("select p from Vehicule p where p.matricule=:x ")
	 Vehicule findByMatricule(@Param("x") String matricule);
	 
	 @Query("select e from Vehicule e where e.active=:x ")
	    List<Vehicule> findActive(@Param("x") boolean active);

}
