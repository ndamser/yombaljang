package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {
	
	@Query("select n from Note n where n.eleve=:x and n.matiere=:y ")
    List<Note> findByEleve(@Param("x") Eleve eleve, @Param("y") Matiere matiere);

	@Query("select n from Note n where n.eleve=:x and n.matiere=:y and n.examen=:z  ")
    Note findByEleveD(@Param("x") Eleve eleve, @Param("y") Matiere matiere, @Param("z") Examen examen);

	@Query("select n from Note n where n.eleve.classe=:x and n.matiere=:y")
    List<Note> findParClasse(@Param("x") Classe classe, @Param("y") Matiere matiere);


    List<Note> findByEleveAndMatiere(Eleve eleve, Matiere matiere);



}
