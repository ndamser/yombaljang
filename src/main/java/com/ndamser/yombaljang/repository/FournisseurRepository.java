package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Fournisseur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface FournisseurRepository extends JpaRepository<Fournisseur, Long> {
	
	@Query("Select d from Fournisseur d where d.nom=:x")
	Fournisseur findByNom(@Param("x") String nom);
	
	@Query("select e from Fournisseur e where e.active=:x ")
    List<Fournisseur> findActive(@Param("x") boolean active);
	
	

}
