package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Gardien;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface GardienRepository extends CrudRepository<Gardien, Long> {
	
	@Query("select e from Gardien e where e.active=:x ")
    List<Gardien> findActive(@Param("x") boolean active);

}
