package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Transport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface TransportRepository extends JpaRepository<Transport, Long> {
	
	@Query("select e from Transport e where e.active=:x ")
    List<Transport> findActive(@Param("x") boolean active);

}
