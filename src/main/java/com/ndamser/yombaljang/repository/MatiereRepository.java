package com.ndamser.yombaljang.repository;

import com.ndamser.yombaljang.model.Matiere;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;



@Repository
public interface MatiereRepository extends JpaRepository<Matiere, Long> {

	@Query("select p from Matiere p where p.nomMatiere=:x ")
	Matiere findByNom(@Param("x") String matiere);
  
}

