package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {

	@Query("Select d from User d where d.userId=:x")
	User findEnabled (@Param("x")  int userId );

	//  @Query("Select d from User d where d.confirmationToken=:x")
	//User findByConfirmationToken (String confirmationToken );

	//  User findByLogin(String login);
	Optional<User> findByUsername(String username);
	  
}
