package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Retard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RetardRepository extends JpaRepository<Retard, Long> {
	
	@Query("select a from Retard a where a.eleve.classe=:x ")
    List<Retard> findParClasse(@Param("x") Classe classe);

}
