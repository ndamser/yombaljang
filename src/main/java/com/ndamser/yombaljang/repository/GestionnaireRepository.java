package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Gestionnaire;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface GestionnaireRepository extends CrudRepository<Gestionnaire, Long> {
	@Query("select e from Gestionnaire e where e.active=:x ")
    List<Gestionnaire> findActive(@Param("x") boolean active);

}
