package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Professeur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ProfesseurRepository extends JpaRepository<Professeur, Long> {
  
	  @Query("select p from Professeur p where p.cin=:x ")
		Professeur findByCin(@Param("x") String matricule);
	  
	  @Query("select e from Professeur  e where e.active=:x ")
      List<Professeur> findActive(@Param("x") boolean active);
	  
}
