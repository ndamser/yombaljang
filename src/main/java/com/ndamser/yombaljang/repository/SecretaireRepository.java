package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Secretaire;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SecretaireRepository extends CrudRepository<Secretaire, Long> {
	
	@Query("select e from Secretaire e where e.active=:x ")
    List<Secretaire> findActive(@Param("x") boolean active);

}
