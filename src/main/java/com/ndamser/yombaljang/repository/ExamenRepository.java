package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Examen;
import com.ndamser.yombaljang.model.Matiere;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ExamenRepository extends JpaRepository<Examen, Long> {

	@Query("select e from Examen e where e.classe=:x ")
    List<Examen> findByClasse(@Param("x") Classe classe);
	
	@Query("select e from Examen e where e.matiere=:x ")
	List<Examen> findParMatiere(@Param("x") Matiere matiere);

}
