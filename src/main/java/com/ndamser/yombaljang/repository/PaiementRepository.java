package com.ndamser.yombaljang.repository;


import com.ndamser.yombaljang.model.Paiement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PaiementRepository extends CrudRepository<Paiement, Long> {

}
