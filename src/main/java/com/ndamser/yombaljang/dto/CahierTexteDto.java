package com.ndamser.yombaljang.dto;


import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Matiere;
import com.ndamser.yombaljang.model.Professeur;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CahierTexteDto {

    private Long idCahierTexte;
    private Professeur professeur;
    private String date;
    private String tache;
    private Matiere matiere;
    private Classe classe;
}
