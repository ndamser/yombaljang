package com.ndamser.yombaljang.dto;

import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Matiere;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RetardDto {
    private Long idRetard;
    private Eleve eleve;
    private Matiere matiere;
}
