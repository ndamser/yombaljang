package com.ndamser.yombaljang.dto;

import com.ndamser.yombaljang.model.CahierTexte;
import com.ndamser.yombaljang.model.Personne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProfesseurDto extends Personne {

    private String cin;
    private String diplome;
    private Date dateDebutTravail;
    private Set<CahierTexte> cahiertextes = new HashSet<CahierTexte>(0);
}
