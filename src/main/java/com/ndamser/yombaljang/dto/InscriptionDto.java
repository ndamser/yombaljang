package com.ndamser.yombaljang.dto;

import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Tarif;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InscriptionDto {

    private Long idInscription;
    private Tarif tarif;
    private Date dateInscription;
    private Eleve eleve;
    private boolean valide;
    private boolean current;
}
