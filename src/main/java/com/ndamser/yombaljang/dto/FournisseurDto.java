package com.ndamser.yombaljang.dto;

import com.ndamser.yombaljang.model.Contact;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FournisseurDto {

    private Long idFournisseur;
    private String adresse;
    private String email;
    private String ville;
    private String telephone;
    private String nom;
    private boolean active;
    private Contact contact;
}
