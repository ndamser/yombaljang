package com.ndamser.yombaljang.dto;


import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Examen;
import com.ndamser.yombaljang.model.Matiere;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NoteDto {
    private Long idNote;
    private Examen examen;
    private String noteExam;
    private String remarque;
    private Eleve eleve;
    private Matiere matiere;
}
