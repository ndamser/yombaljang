package com.ndamser.yombaljang.dto;


import com.ndamser.yombaljang.model.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EleveDto extends Personne {

    private Classe classe;
    private Set<Inscription> inscriptions = new HashSet<Inscription>(0);
    private Niveau niveau;
    private int typePaiement;
    private Set<Responsable> responsables = new HashSet<Responsable>(0);
    private String matricule;
    private Set<Absence> absences = new HashSet<Absence>(0);
    private Set<Note> notes = new HashSet<Note>(0);
}
