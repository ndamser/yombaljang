package com.ndamser.yombaljang.dto;

import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Matiere;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AbsenceDto {

    private Long idAbsence;
    private Eleve eleve;
    private Date dateAbsence;
    private Matiere matiere;
    private boolean justifie;
}
