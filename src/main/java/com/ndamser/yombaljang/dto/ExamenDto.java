package com.ndamser.yombaljang.dto;


import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Matiere;
import com.ndamser.yombaljang.model.Note;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExamenDto {

    private Long idExamen;
    private Matiere matiere;
    private Classe classe;
    private String numExamen;
    private Integer pourcentage;
    private Set<Note> notes = new HashSet<Note>(0);
    private boolean valide;
    private Date date;
    private String duree;
}
