package com.ndamser.yombaljang.dto;

import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Matiere;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ObservationDto {

    private Long idObservation;
    private Eleve eleve;
    private String remarque;
    private Date date;
    private Matiere matiere;
}
