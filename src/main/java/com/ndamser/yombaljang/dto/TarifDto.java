package com.ndamser.yombaljang.dto;


import com.ndamser.yombaljang.model.Garde;
import com.ndamser.yombaljang.model.Inscription;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TarifDto {

    private Long idTarif;
    private int montant;
    private String nomTarif;
    private Set<Garde> gardes = new HashSet<Garde>(0);
    private Set<Inscription> inscriptions = new HashSet<Inscription>(0);
}

