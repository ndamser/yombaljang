package com.ndamser.yombaljang.dto;


import com.ndamser.yombaljang.model.Personne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SecretaireDto extends Personne {

    private String cin;
    private Date dateDebutTravail;
}
