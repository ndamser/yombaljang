package com.ndamser.yombaljang.dto;


import com.ndamser.yombaljang.model.Aide;
import com.ndamser.yombaljang.model.Professeur;
import com.ndamser.yombaljang.model.Tarif;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GardeDto {

    private Long idGarde;
    private Aide aide;
    private Professeur professeur;
    private Date dateDebut;
    private Date dateFin;
    private Tarif tarif;
    private boolean active;
}
