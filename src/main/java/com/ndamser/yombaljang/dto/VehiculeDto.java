package com.ndamser.yombaljang.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VehiculeDto {
    private Long idVehicule;
    private String matricule;
    private String modele;
    private Date dateDachat;
    private boolean active;
    private String marque;
}
