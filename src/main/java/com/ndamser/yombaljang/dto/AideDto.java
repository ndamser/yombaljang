package com.ndamser.yombaljang.dto;


import com.ndamser.yombaljang.model.Garde;
import com.ndamser.yombaljang.model.Personne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AideDto extends Personne {

    private String cin;
    private Date dateDebutTravail;
    private Set<Garde> gardes = new HashSet<Garde>(0);
}
