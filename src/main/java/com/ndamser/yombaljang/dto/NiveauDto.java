package com.ndamser.yombaljang.dto;


import com.ndamser.yombaljang.model.Eleve;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NiveauDto {

    private Long idNiveau;
    private String niveau;
    private Set<Eleve> eleves = new HashSet<Eleve>(0);

}
