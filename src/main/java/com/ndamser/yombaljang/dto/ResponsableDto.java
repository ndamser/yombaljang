package com.ndamser.yombaljang.dto;

import com.ndamser.yombaljang.model.Eleve;
import com.ndamser.yombaljang.model.Personne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponsableDto extends Personne {

    private String cin;
    private String lienParente;
    private Set<Eleve> eleves = new HashSet<Eleve>(0);
}
