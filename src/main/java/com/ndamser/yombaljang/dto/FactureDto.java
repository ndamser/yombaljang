package com.ndamser.yombaljang.dto;

import com.ndamser.yombaljang.model.Fournisseur;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FactureDto {

    private Long idFacture;
    private Fournisseur fournisseur;
    private int numeroFacture;
    private String produit;
    private float nombreProduit;
    private float prixHT;
    private float totalHT;
    private boolean active;
    private Date date;
}
