package com.ndamser.yombaljang.dto;


import com.ndamser.yombaljang.model.Classe;
import com.ndamser.yombaljang.model.Matiere;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DevoirDto {
    private Long idDevoir;
    private String tache;
    private Date dateDevoir;
    private Classe classe;
    private Matiere matiere;
    private boolean valide;
}
